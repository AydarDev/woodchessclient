﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketModels;
using WebSocketSharp;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.IO;

namespace Assets.Scripts
{
    public class WsClientKeeper : MonoBehaviour
    {
        internal WebSocketClient WsClient => WebSocketClient.GetInstance();
        internal Action SubscribeReceiver;
        internal Action<string> ShowMessage;
        Coroutine MonitorCoroutine;
        WaitForSeconds oneSecond = new WaitForSeconds(1.0f);
        WaitForSeconds halfSecond = new WaitForSeconds(0.5f);
        string userDataFile;


        private void Start()
        {
            userDataFile = Application.persistentDataPath + WoodChess.userDataFile;
            InitWebSocket();
            MonitorCoroutine = StartCoroutine(MonitorConnection());            
        }

        private IEnumerator MonitorConnection()
        {
            while (true)
            {
                yield return oneSecond;
                if (WsClient.webSocket.ReadyState != WebSocketState.Open && 
                    WsClient.webSocket.ReadyState != WebSocketState.Connecting)
                {
                    if (ShowMessage != null)
                        ShowMessage("Connecting...");
                    InitWebSocket();
                }
            }
        }

        internal void InitWebSocket()
        {
            WsClient.InitWebSocket(WoodChess.gameServer);
            WsClient.webSocket.OnOpen += (object sender, EventArgs e) =>
            {
                if (SubscribeReceiver != null)
                    SubscribeReceiver();
                Debug.Log("Websocket connected!");
            }; 
            WsClient.webSocket.OnClose += (object sender, CloseEventArgs e) => Debug.Log("Websocket disconnected.");
        }

        internal void StopConnectionMonitoring()
        {
            StopCoroutine(MonitorCoroutine);
        }

        internal void TryLogin(string json = "")
        {
            string[] creds;
            if ((creds = FileHelper.GetCredsFromFile()) != null)
            {
                ShowMessage?.Invoke("Authenticating...");
                string login = EncryptionHelper.Encrypt(creds[0], EncryptionHelper.networkKey);
                string password = EncryptionHelper.Encrypt(creds[1], EncryptionHelper.networkKey);
                StartCoroutine(WaitWsAndRun(WsClient.Login, login, password));
            }

            else
            {
                if (File.Exists(userDataFile))
                    File.Delete(userDataFile);
                SceneManager.LoadScene("EntryScene");
            }
        }

        internal IEnumerator WaitWsAndRun(Action<string[]> action, params string[] arguments)
        {
            int counter = 0;
            WaitForSeconds halfSecond = new WaitForSeconds(0.5f);
            while (WsClient.webSocket.ReadyState != WebSocketState.Open)
            {
                Debug.Log("Waiting for websocket opening...");

                Debug.Log(counter);
                if (++counter > 10)
                    throw new Exception("Cannot connect to server!");

                yield return halfSecond;
            }
            action(arguments);
        }


    }
}