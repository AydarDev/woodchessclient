﻿using System;
using System.Collections;
using Assets.Scripts;
using ChessRules;
using Globals;
using UnityEngine;
using WebSocketModels.Clocks;
using WebSocketModels.Models;

namespace Clocks
{
    class UnityChessClock : ChessClock
    {
        public override TimeSpan CorrectionSpan => WoodChess.GetInstance().CorrectionSpan; 
        public UnityChessClock(GameDto game, Chess boardChess) : base(game, boardChess)
        {
        }
        
        internal IEnumerator ClockRunCoroutine()
        {
            WaitForSeconds oneHundredMilliseconds = new WaitForSeconds(0.1f);

            while (IsRun)
            {
                foreach (Timer timer in Timers.Values)
                    timer.Update();
                yield return oneHundredMilliseconds;
            }
        }
    }
}