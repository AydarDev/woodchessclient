﻿using System;
using System.Collections.Generic;
using Configuration;
using Helpers;
using Newtonsoft.Json;
using UnityEngine;
using WebSocketModels.Enums;
using WebSocketModels.Models;
using WebSocketModels.Models.NullClasses;
using WebSocketModels.Models.Puzzles;

namespace Globals
{
    internal class WoodChess
    {
        private static WoodChess _instance;
        internal GameConfiguration GameConfiguration { get; private set; }
        internal PlayerDto CurrPlayer { get; set; }
        internal GameDto CurrGame { get; set; }
        internal PlayerDto CurrOpponent {
            get
            {
                if (this.CurrGame == null || CurrGame.IsNull)
                    return NullPlayer.GetInstance();
                else
                    return CurrPlayer.Equals(CurrGame.WhitePlayer) ? CurrGame.BlackPlayer : CurrGame.WhitePlayer;
            }
        }
        internal TimeSpan CorrectionSpan;
        private DateTime ServerTime { get; set; }

        internal Dictionary<PuzzleType, PuzzleDto> CurrentPuzzles { get; set; } = new Dictionary<PuzzleType, PuzzleDto>();
        internal string Token { get; set; }

        private WoodChess()
        {
        }

        public static WoodChess GetInstance()
        {
            return _instance ?? (_instance = new WoodChess());
        }

        internal void ResetCurrPlayer()
        {
            CurrPlayer = NullPlayer.GetInstance();
        }

        internal void SetCurrGame(string json)
        {
            var game = JsonConvert.DeserializeObject<GameDto>(json);
            Debug.Log($"Set current game {game}");
            CurrGame = game;
        }
        internal void ResetCurrGame()
        {
            CurrGame = NullGame.GetInstance();
        }

        internal void SynchronizeClockWs(DateTime serverTime)
        {
            ServerTime = serverTime;
            CorrectionSpan = ServerTime - DateTime.UtcNow;
            Debug.Log($"Get time sync: {ServerTime}\n Correction span:{CorrectionSpan}");
        }

        public void InitConfiguration()
        {
            GameConfiguration = ConfigurationHelper.LoadConfiguration();
            if (GameConfiguration == null)
            {
                GameConfiguration = GameConfiguration.GetDefault();
                ConfigurationHelper.SaveConfiguration();
            }
        }
    }
}