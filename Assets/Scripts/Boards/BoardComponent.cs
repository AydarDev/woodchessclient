﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ChessRules;
using Globals;
using GO_Scripts;
using UnityEngine;
using WebSocketModels.Models;

namespace Boards
{
    public class BoardComponent : MonoBehaviour
    {
        public GameObject figure;
        public GameObject promotesCanvas;

        private readonly Dictionary<string, string> castlingMoves = new Dictionary<string, string>()
        {
            { "Ke1g1", "Rh1f1"}, { "Ke1c1", "Ra1d1" }, { "ke8g8", "rh8f8" }, { "ke8c8", "ka8d8" }
        };

        internal UnityBoard board;
        private float speed => WoodChess.GetInstance()
            .GameConfiguration.BoardSettings.AnimationSpeed;

        private void Awake()
        {
            board = new UnityBoard(this);
        }

        internal void SetupBoard(string fen, SideColor sideColor = SideColor.White, string lastMove = "")
        {
            board.SetUpBoard(fen, sideColor, lastMove);
        }
        
        internal void ShowPositionAfterMove(string move)
        {
            StartCoroutine(ShowPositionAfterMoveImpl(move));
        }

        internal void ShowPositionAfterMove(MoveDto move, Action<MoveDto> callback = null)
        {
            StartCoroutine(ShowPositionAfterMoveImpl(move, callback));
        }

        private IEnumerator ShowPositionAfterMoveImpl(string move, Action<string> callback = null)
        {
            yield return ProceedMove(move);
            if (callback != null) callback(move);
        }

        private IEnumerator ShowPositionAfterMoveImpl(MoveDto move, Action<MoveDto> callBack = null)
        {
            yield return ProceedMove(move.Value);
            if (callBack != null) callBack(move);
        }

        private IEnumerator ProceedMove(string move)
        {
            board.UpdateBoardState(move);
            board.LastMove = move;
            if (board.IsPromote) board.HidePromotionFigures();

            yield return AnimateMove(move);
            if (IsCastling(move))
                yield return AnimateMove(castlingMoves[move]);
        }

        private IEnumerator AnimateMove(string move)
        {
            var fromSquare = board.Squares[move.Substring(1, 2)];
            var toSquare = board.Squares[move.Substring(3, 2)];
            var currFigure = board.SelectedFigure ? 
                board.SelectedFigure.transform : fromSquare.CurrentFigure.transform;
            
            yield return AnimateMove(currFigure, fromSquare, toSquare);
            
            if (board.IsPromotionMove(move)) 
                board.PromotePawn(currFigure.gameObject, move.Substring(5, 1));
        }

        internal IEnumerator AnimateMove(Transform currFigure, Square fromSquare, Square toSquare)
        {
            while (currFigure.position != toSquare.transform.position)
            {
                currFigure.position =
                    Vector2.MoveTowards(currFigure.position,
                        toSquare.transform.position, speed * Time.deltaTime);
                yield return Time.deltaTime;
            }
            
            SwapFigures(fromSquare, toSquare, currFigure);
            board.UnselectFigure();
            board.MarkLastMove();
        }

        private bool IsCastling(string move)
        {
            return castlingMoves.Keys.Contains(move);
        }

        private static void SwapFigures(Square fromSquareObject, Square toSquareObject, Transform figureObject)
        {
            fromSquareObject.GetComponent<Square>().CurrentFigure = null;
            if (toSquareObject.GetComponent<Square>().CurrentFigure)
                Destroy(toSquareObject.GetComponent<Square>().CurrentFigure.gameObject);
            toSquareObject.GetComponent<Square>().CurrentFigure = figureObject.GetComponent<Figure>();
            figureObject.GetComponent<Figure>().CurrentSquare = toSquareObject.GetComponent<Square>();
        }

        internal void ResetFigurePosition()
        {
            if (board.SelectedFigure)
                ResetFigurePosition(board.SelectedFigure);
        }

        internal void ResetFigurePosition(Figure currFigure)
        {
            StartCoroutine(ResetFigureImpl(currFigure));
        }

        private IEnumerator ResetFigureImpl(Figure currFigure)
        {
            currFigure.transform.position = currFigure.CurrentSquare.transform.position;
            board.UnselectFigure();
            yield break;
        }
    }
}