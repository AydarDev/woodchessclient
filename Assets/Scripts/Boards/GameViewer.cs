﻿using System.Collections;
using GameInstruments;
using GameInstruments.Models;
using GO_Scripts;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Linq;
using ChessRules;

namespace Boards
{
    public class GameViewer : MonoBehaviour
    {
        public GameObject forwardButton;
        public GameObject backwardButton;

        private BoardComponent boardComponent => GameObject.Find("Board").GetComponent<BoardComponent>();
        private UnityBoard board => boardComponent.board;
        private GameParser gameParser;
        private GameInfo gameInfo;
        private const string PgnName = "/sample.pgn";
        private Ply currPly = NullPly.GetInstance();
        private Ply previousPly = NullPly.GetInstance();
        private SideColor color => boardComponent.board.BoardState.Color;

        private void Start()
        {
            gameParser = new GameParser();
            GetGameTreeFromFile();
            SetButtons();
            board.SetUpBoard(gameInfo.InitialFen);
            currPly = gameInfo.GameTree[0];
        }

        private void Update()
        {
            if(Input.GetKeyUp(KeyCode.LeftArrow))
                MoveBackwardPush();
            if(Input.GetKeyUp(KeyCode.RightArrow))
                MoveForwardPush();
        }

        private void SetButtons()
        {
            forwardButton.GetComponent<ButtonScript>().action = MoveForwardPush;
            backwardButton.GetComponent<ButtonScript>().action = MoveBackwardPush;
            forwardButton.GetComponent<ButtonScript>().holdAction = () => { };//StartCoroutine(MoveForwardHold());
            backwardButton.GetComponent<ButtonScript>().holdAction = () => { };//StartCoroutine(MoveBackwardHold());
            backwardButton.GetComponent<Button>().interactable = false;
        }

        private void GetGameTreeFromFile()
        {
            string pgn = File.ReadAllText(Application.persistentDataPath + PgnName);
            gameInfo = gameParser.ParsePgn(pgn);
        }

        private IEnumerator MoveForwardHold()
        {
            Debug.Log("Hold forward");
            yield break;
        }

        private IEnumerator MoveBackwardHold()
        {
            Debug.Log("Hold backward");
            yield break;
        }
        
        private void MoveForwardPush()
        {
            if (!currPly.IsNull)
                StartCoroutine(MoveForward());
        }

        private void MoveBackwardPush()
        {
            if (!previousPly.IsNull)
                StartCoroutine(MoveBackward());
        }
        
        private IEnumerator MoveForward()
        {
            var move = currPly.Move;
            board.LastMove = move;
            var fromSquare = board.Squares[move.Substring(1, 2)];
            var toSquare = board.Squares[move.Substring(3, 2)];
            var figure = fromSquare.CurrentFigure.transform;
            yield return boardComponent.AnimateMove(figure, fromSquare, toSquare);
            
            if (board.IsPromotionMove(move)) 
                board.PromotePawn(figure.gameObject, move.Substring(5, 1));
            
            previousPly = currPly;
            currPly = currPly.NextPlies.Any() ? currPly.NextPlies[0] : NullPly.GetInstance();
            SetButtonsInteraction();
        }

        private IEnumerator MoveBackward()
        {
            var move = previousPly.Move;
            var fromSquare = board.Squares[move.Substring(3, 2)];
            var toSquare = board.Squares[move.Substring(1, 2)];  
            var figure = fromSquare.CurrentFigure.transform;
            yield return boardComponent.AnimateMove(figure, fromSquare, toSquare);
            currPly = !previousPly.IsNull ? previousPly : currPly;
            previousPly = currPly.PreviousPly;
            SetupBoard();
            SetButtonsInteraction();
        }
        
        private void SetupBoard()
        {
            if (!previousPly.IsNull)
                boardComponent.SetupBoard(previousPly.Fen, color,
                    previousPly.Move);
            else
                boardComponent.SetupBoard(gameInfo.InitialFen, gameInfo.PlayerColor);
        }

        private void SetButtonsInteraction()
        {
            forwardButton.GetComponent<Button>().interactable = !currPly.IsNull;
            backwardButton.GetComponent<Button>().interactable = !previousPly.IsNull;
        }
    }
}