﻿using WebSocketModels.WsReceivers;

namespace Boards
{
    interface IBoardReceiver : IPlayerReceiver, IGameReceiver, IDataReceiver
    {
    }
}
