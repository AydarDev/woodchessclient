﻿using System;
using System.Collections.Generic;
using System.Text;
using ChessRules;
using Configuration;
using Globals;
using GO_Scripts;
using Helpers;
using UnityEngine;
using UnityEngine.UI;
using WebSocketModels.Boards;
using Object = UnityEngine.Object;

namespace Boards
{
    public class UnityBoard : BaseBoard<Square, Figure>
    {
        private const string Figures = "Figures";
        private const float SquareSize = 100;
        private const float Offset = 350;
        private readonly Vector3 scale = new Vector3(50, 50, 0);
        private readonly BoardComponent boardComponent;
        private GameObject LowHorizontalEdge => GameObject.Find("LowHorizontalEdge");
        private GameObject LeftVerticalEdge => GameObject.Find("LeftVerticalEdge");
        private Transform figuresPanel => GameObject.Find("FiguresPanel").transform;
        private ButtonScript RotateButton => GameObject.Find("RotateButton").GetComponent<ButtonScript>();

        private GameObject promoteCanvasImpl;
        private BoardSettings boardSettings => WoodChess.GetInstance().GameConfiguration.BoardSettings;

        internal UnityBoard(BoardComponent boardComponent)
        {
            this.boardComponent = boardComponent;
            RotateButton.action = Rotate;
            Log = SystemHelper.Instance.Log;
        }
        
        
        public override void MakeCaptions()
        {
            for (int i = 0; i < 8; i++)
            {
                LowHorizontalEdge.transform.Find($"Col{i}").GetComponent<Text>().text =
                    BoardState.XToRank(i);
                LeftVerticalEdge.transform.Find($"Row{i}").GetComponent<Text>().text =
                    BoardState.YToRow(i);
            }
        }
        
        public override void SetUpBoardImpl()
        {
            InitSquares();
            ClearBoard();
            CreateFigures();
        }

        private void InitSquares()
        {
            Squares.Clear();
            foreach (Transform squareTransform in GameObject.Find("BoardPanel").transform)
            {
                var square = squareTransform.gameObject.GetComponent<Square>();
                var squareName = BoardState.CoordsToSquare((square.x, square.y));
                Squares[squareName] = square;
                square.name = squareName;
                square.GetComponent<Square>().board = this;
            }
        }

        void CreateFigures()
        {
            for (int x = 0; x < 8; x++)
            for (int y = 0; y < 8; y++)
            {
                var squareName = BoardState.CoordsToSquare((x, y));
                string figureName = BoardState.GetFigureAt(Chess, x, y);
                if(".".Equals(figureName)) continue;
                var figureObject = CreateFigureObject(figureName, (x, y));
                
                var square = Squares[squareName].GetComponent<Square>();
                figureObject.GetComponent<Figure>().CurrentSquare = square;
                square.CurrentFigure = figureObject.GetComponent<Figure>();
            }
        }

        private void ClearBoard()
        {
            if (figuresPanel)
            {
                foreach (Transform child in figuresPanel)
                    Object.Destroy(child.gameObject);
            }
        }

        public override void PickFigure(Figure figure)
        {
            if (IsFigureSelected)
            {
                var move = new StringBuilder(SelectedFigure.FigureSquare).Append(figure.CurrentSquare.name).ToString();
                if (IsValidMove(move))
                    MakeMove(move);
                else UnselectFigure();
            }
        }
        
        public void DropFigure(Square square)
        {
            UnmarkSquares();
            var move = new StringBuilder().Append(SelectedFigure.FigureSquare).Append(square.name).ToString();
            if (IsValidMove(move))
                MakeMove(move);
            else
                ResetObject(SelectedFigure);
        }
        
        public override void MakeMove(string move)
        {
            if (IsPromotionMove(move) && !IsPromote)
            {
                IsPromote = true;
                if (boardSettings.AutoPromotionToQueen)
                {
                    var queen = SelectedFigure.Color.Equals(SideColor.Black) ? "q" : "Q";
                    BoardConsumer.SendMove(move + queen);
                }
                else
                {
                    ShowPromotionFigures(move);
                }
            }
            else
            {
                BoardConsumer.SendMove(move);
            }
        }

        
        internal void SelectFigure(Figure figure)
        {
            if (SelectedFigure == figure) return;
            if (SelectedFigure)
                SelectedFigure.IsSelected = false;
            figure.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 2;
            SelectedFigure = figure;
            MarkSquaresTo(figure.FigureSquare);
            ShowSquare(figure.CurrentSquare.x, figure.CurrentSquare.y, true);
            MarkLastMove();
        }

        internal void UnselectFigure()
        {
            if (SelectedFigure)
            {
                SelectedFigure.IsSelected = false;
                SelectedFigure.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 1;
            }
                
            SelectedFigure = null;
            UnmarkSquares();
            MarkLastMove();
        }

        internal string VectorToSquare(Vector2 point)
        {
            var coords = VectorToDigitCoords(point); 
            return BoardState.CoordsToSquare(coords);
        }

        private static (int, int) VectorToDigitCoords(Vector2 point)
        {
            var x = (int)Math.Floor(Math.Abs((point.x + 8) / 2));
            var y = (int)Math.Floor(Math.Abs((point.y + 8) / 2));

            return (x, y);
        }

        public override void ResetObject(Figure figure)
        {
            boardComponent.ResetFigurePosition(figure);
        }

        public override bool IsValidPick(Figure figure)
        {
            return figure.Color == Chess.MoveColor &&
                   BoardConsumer.IsValidPick(figure.Color);
        }

        private GameObject CreateFigureObject(string pattern, (int x, int y) coords, int sortOrder = 1, bool isPromote = false)
        {
            var newFigure = Object.Instantiate(boardComponent.figure, figuresPanel, true);

            newFigure.transform.localPosition = new Vector2(coords.x * SquareSize - Offset, coords.y * SquareSize - Offset);
            newFigure.transform.localScale = scale;
            SetFigureSprite(newFigure, $"{pattern}");
            var figure = newFigure.GetComponent<Figure>();
            figure.SetFigureInfo(pattern);
            figure.Board = this;
            figure.IsPromotionFigure = isPromote;
            figure.GetComponent<SpriteRenderer>().sortingOrder = sortOrder;
            return newFigure;
        }

        private void SetFigureSprite(GameObject go, string source)
        {
            go.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>($"{Figures}/{FigureNames[source]}");
        }

        public override void ShowSquare(int x, int y, bool marked = false)
        {
            string square = (x + y) % 2 == 0 ? "BlackSquare" : "WhiteSquare";
            if (marked)
                square += "Marked";

            var key = BoardState.CoordsToSquare((x, y));
            Squares[key].GetComponent<Image>().sprite = Resources.Load<Sprite>($"Squares/{square}");
        }
        
        public override void ShowPromotionFigures(string move)
        {
            PromotionWizard.InitPromotion(move);
            ShowPromotionCanvas();
            foreach (KeyValuePair<string, (int x, int y)> promoteInfo in PromotionWizard.PromotesInfo)
            {
                var promote =
                    CreateFigureObject(promoteInfo.Key, (promoteInfo.Value.x, promoteInfo.Value.y), 4, true);
                Promotes.Add(promote.GetComponent<Figure>());
            }

            UnmarkSquares();
        }

        public override void HidePromotionFigures()
        {
            PromotionWizard.OnPromotionMove = "";
            foreach (var promote in Promotes)
                if (promote.gameObject)
                    Object.Destroy(promote.gameObject);
            
            Promotes.Clear();
            HidePromotionCanvas();
        }

        internal void ChoosePromotionFigure(Figure figure)
        {
            var move = PromotionWizard.OnPromotionMove + figure.name;
            if(IsValidMove(move))
                MakeMove(move);
        }

        internal void PromotePawn(GameObject promotePawn, string promoteFigure)
        {
            SetFigureSprite(promotePawn, promoteFigure);
            promotePawn.name = promoteFigure;
            IsPromote = false;
        }
        private void ShowPromotionCanvas()
        {
            var layoutName = $"{Chess.MoveColor}PromoteLayout";
            promoteCanvasImpl = Object.Instantiate(boardComponent.promotesCanvas, figuresPanel);
            float[] coords = PromotionWizard.GetLayoutPosition();
            Debug.Log($"Result Coords x:{coords[0]}, y:{coords[1]}");

            promoteCanvasImpl.transform.localPosition =
                new Vector2(coords[0] * SquareSize - Offset, coords[1] * SquareSize - Offset);
            promoteCanvasImpl.transform.localScale = scale;

            var path = $"Promotes/{layoutName}";
            Debug.Log($"Get promotionLayout by path {path}");
            promoteCanvasImpl.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(path);
        }

        private void HidePromotionCanvas()
        {
            if (promoteCanvasImpl) Object.Destroy(promoteCanvasImpl);
        }
    }
}