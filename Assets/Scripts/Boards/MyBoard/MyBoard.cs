﻿using ChessRules;
using UnityEngine;
using WebSocketModels.Boards;

namespace Boards.MyBoard
{
    public class MyBoard : MonoBehaviour, IBoardConsumer
    {
        private BoardComponent boardComponent => GameObject.Find("Board").GetComponent<BoardComponent>();
        private UnityBoard board => boardComponent.board;

        private void Start()
        {
            
            board.BoardConsumer = this;
        }

        public bool IsMyMove()
        {
            return true;
        }

        public void Rotate()
        {
            
        }

        public void SendMove(string move)
        {
            boardComponent.ShowPositionAfterMove(move);
        }

        public bool IsValidPick(SideColor figureColor)
        {
            return true;
        }
    }
}