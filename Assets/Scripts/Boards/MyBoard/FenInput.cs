﻿using System;
using Boards;
using Helpers;
using UnityEngine;
using UnityEngine.UI;

namespace GO_Scripts.MyBoard
{
    public class FenInput : MonoBehaviour
    {
        public GameObject board;
        private BoardComponent boardComponent => board.GetComponent<BoardComponent>();
        private Text fenInputText => transform.Find("FenInputText").GetComponent<Text>();

        public void SetUpBoard()
        {
            try
            {
                boardComponent.SetupBoard(fenInputText.text);
            }
            catch (Exception)
            {
                SystemHelper.Instance.ShowMessage("Wrong FEN", true);
            }
        }
    }
}
