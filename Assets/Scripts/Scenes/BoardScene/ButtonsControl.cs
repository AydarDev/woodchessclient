﻿using Boards;
using Globals;
using GO_Scripts;
using GO_Scripts.Toggles;
using UnityEngine;
using WebSocketModels;
using WebSocketModels.Enums;
using WebSocketModels.Models;

namespace Scenes.BoardScene
{
    public class ButtonsControl : MonoBehaviour
    {
        private WoodChess WoodChess => WoodChess.GetInstance();
        private NetworkClient NetworkClient => NetworkClient.GetInstance();
        private GameObject ResignButtonObject => transform.Find("ResignButton").gameObject;
        private GameObject DrawButtonObject => transform.Find("DrawButton").gameObject;
        private ButtonWithConfirm DrawButton => DrawButtonObject.GetComponent<ButtonWithConfirm>();
        private ButtonWithConfirm ResignButton => ResignButtonObject.GetComponent<ButtonWithConfirm>();
        private ButtonScript RotateButton => GameObject.Find("RotateButton").GetComponent<ButtonScript>();
        private UnityBoard Board => GameObject.Find("Board").GetComponent<BoardComponent>().board;

        private void Start()
        {
            SetupButtonsActions();
        }

        private void SetupButtonsActions()
        {
            ResignButton.action = Resign;
            DrawButton.action = OfferDraw;
            RotateButton.action = Board.Rotate;
        }

        private void Resign()
        {
            NetworkClient.ResignGame(WoodChess.CurrGame.Id);
        }

        private void OfferDraw()
        {
            NetworkClient.OfferDraw(WoodChess.CurrGame.Id);
        }

        public void SetDrawButtons(DrawInfoDto drawInfo)
        {
            switch (drawInfo.DrawFlag)
            {
                case DrawFlag.Offer:
                {
                    if (drawInfo.FromPlayerId != WoodChess.GetInstance().CurrPlayer.Id)
                    {
                        if (DrawButton.IsBlocked)
                            DrawButton.Unblock();
                    }
                    else
                        DrawButton.Block();
                    break;
                }
                case DrawFlag.Decline:
                    if (drawInfo.BlockCounter > 0 && drawInfo.FromPlayerId != WoodChess.CurrPlayer.Id)
                        DrawButton.Block();
                    break;
                case DrawFlag.None:
                    if (DrawButton.IsBlocked)
                        DrawButton.Unblock();
                    break;
            }
        }

        public void BlockButtons()
        {
            ResignButton.Block();
            DrawButton.Block();
        }
    }
}