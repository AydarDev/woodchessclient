﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Boards;
using ChessRules;
using Clocks;
using Globals;
using GO_Scripts;
using GO_Scripts.ConfirmMenu;
using GO_Scripts.Toggles;
using Helpers;
using NetworkServices;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using WebSocketModels;
using WebSocketModels.Boards;
using WebSocketModels.Enums;
using WebSocketModels.Models;
using WebSocketModels.WsMessages.WsResponses;
using static ChessRules.SideColor;


[assembly: InternalsVisibleTo("Tests")]
namespace Scenes.BoardScene
{
    public class BoardScene : MonoBehaviour, IBoardReceiver, IBoardConsumer
    {
        public GameObject firstPlayerPanelGo;
        public GameObject secondPlayerPanelGo;
        public GameObject ConfirmMenuObject;
        
        private PlayerPanel firstPlayerPanel => firstPlayerPanelGo.GetComponent<PlayerPanel>();
        private PlayerPanel secondPlayerPanel => secondPlayerPanelGo.GetComponent<PlayerPanel>();
        private ButtonsControl ButtonsControl => GameObject.Find("MenuButtonsCanvas").GetComponent<ButtonsControl>();

        private List<PlayerPanel> playerPanels;
        private Text gameHeader => GameObject.Find("GameHeader").GetComponent<Text>();
        internal UnityChessClock ChessClock { get; private set; }
        BoardComponent boardComponent => GameObject.Find("Board").GetComponent<BoardComponent>();
        WoodChess woodChess => WoodChess.GetInstance();
        private MenuToggle settingsButton => GameObject.Find("SettingsButton").GetComponent<MenuToggle>();
        private ConfirmMenuScript confirmMenu => ConfirmMenuObject.GetComponent<ConfirmMenuScript>();

        private GameDto CurrGame
        {
            get => woodChess.CurrGame;
            set => woodChess.CurrGame = value;
        }

        private PlayerDto currPlayer => woodChess.CurrPlayer;

        private SideColor toMoveSide => board.Chess.MoveColor;
        public bool IsMyMove() => myColor == toMoveSide;
        private SideColor myColor => CurrGame.BlackPlayer.Equals(currPlayer) ? Black : White;
        private SideColor opponentColor => myColor.Flip();
        private readonly Dictionary<SideColor, PlayerDto> gameMembers = new Dictionary<SideColor, PlayerDto>();
        NetworkClient networkClient => NetworkClient.GetInstance();
        private bool isRotate => board.IsRotate;
        private UnityBoard board => boardComponent.board;
        private static WebClient webClient => WebClient.Instance;

        private void Start()
        {
            playerPanels = new List<PlayerPanel>() {firstPlayerPanel, secondPlayerPanel};
            settingsButton.action = ToggleSettingsMenu;
            WsClient.Instance.WsReceiver = this;
            webClient.GetGame(CurrGame.Id, HandleGame);
            SetupBoard();
            Debug.Log("Creating new clock");
            ChessClock = new UnityChessClock(CurrGame, board.Chess);
            SetGameInfo();
            StartChessClock();
            ButtonsControl.SetDrawButtons(CurrGame.DrawInfo);
        }

        private void SetupBoard()
        {
            board.BoardConsumer = this;
            board.SetUpBoard(CurrGame.Fen, myColor, CurrGame.LastMove);
        }


        private void Update()
        {
            try
            {
                if (Input.GetKey(KeyCode.Escape))
                {
                    Exit();
                    SceneManager.LoadScene("MainScene");
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        void Exit()
        {
            StopAllCoroutines();
            if (CurrGame.Status == GameStatus.Done)
            {
                networkClient.ExitGame(woodChess.CurrGame.Id);
                woodChess.ResetCurrGame();
            }
        }
        private void StartChessClock()
        {
            StartCoroutine(ChessClock.ClockRunCoroutine());
            ChessClock.StartClock();
        }

        private void SetGameInfo()
        {
            SetGameMembers();
            ViewGameHeaderInfo();
            InitPlayerPanels();
        }

        private void SetGameMembers()
        {
            PlayerDto currOpponent = currPlayer.Equals(CurrGame.WhitePlayer) ? CurrGame.BlackPlayer : CurrGame.WhitePlayer;
            gameMembers[myColor] = currPlayer;
            gameMembers[opponentColor] = currOpponent;
        }

        private void InitPlayerPanels()
        {
            if (!isRotate)
            {
                firstPlayerPanel.Color = myColor;
                secondPlayerPanel.Color = opponentColor;
            }
            else
            {
                secondPlayerPanel.Color = myColor;
                firstPlayerPanel.Color = opponentColor;
            }

            foreach (var playerPanel in playerPanels) playerPanel.SetPlayerData(gameMembers[playerPanel.Color]);
        }

        public void Rotate()
        {
            InitPlayerPanels();
        }

        private void ViewGameHeaderInfo()
        {
            gameHeader.text =
                $"Game {gameMembers[White].Name} vs {gameMembers[Black].Name} {CurrGame.TimeControl.TimeRange}'+{CurrGame.TimeControl.TimeIncrement}''";
        }
        
        public void HandlePlayer(PlayerResponse playerReponse)
        {
            var player = playerReponse.Player;
            if (firstPlayerPanel.Player.Equals(player))
                firstPlayerPanel.UpdatePlayerInfo(player);
            if (secondPlayerPanel.Player.Equals(player))
                secondPlayerPanel.UpdatePlayerInfo(player);
        }

        public void HandleRatingDeviation(PlayerResponse playerResponse)
        {
            throw new NotImplementedException();
        }

        private void HandleGame(GameDto inputGame)
        {
            if (inputGame.Id != this.CurrGame.Id)
                return;

            if (this.CurrGame.LastMove != inputGame.LastMove)
            {
                boardComponent.ShowPositionAfterMove(inputGame.LastMove);
                SwapClock(this.CurrGame.TimeInfo);
            }

            Debug.Log("Sync clock in HandleGame");
            this.CurrGame = inputGame;
                
            if (!IsGamePlaying())
                FinalizeGame(inputGame);
        }
        
        public void HandleGame(GameResponse gameResponse)
        {
            try
            {
                var inputGame = gameResponse.Game;
                HandleGame(inputGame);
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        void FinalizeGame(GameDto finGame)
        {
            ChessClock.StopClock();
            ChessClock.Synchronize(finGame.TimeInfo);
            ShowGameResult();
            UpdatePlayersRatings(finGame);
            ButtonsControl.BlockButtons();
        }

        private void UpdatePlayersRatings(GameDto finGame)
        {
            firstPlayerPanel.ShowRatingDeviation(finGame.GamePlayers.First(gp => gp.Color == firstPlayerPanel.Color).RatingDeviation.Deviation);
            secondPlayerPanel.ShowRatingDeviation(finGame.GamePlayers.First(gp => gp.Color == secondPlayerPanel.Color).RatingDeviation.Deviation);

            var newRating = finGame.GamePlayers.First(gp => gp.Color == myColor).RatingDeviation.NewRating;
            woodChess.CurrPlayer.SetRating(this.CurrGame.RatingType, newRating);
        }

        void ShowGameResult()
        {
            StringBuilder newResult = new StringBuilder();

            if (CurrGame.Status != GameStatus.Done) return;
            if ((CurrGame.Result == GameResult.WhiteWin && myColor == White) ||
                (CurrGame.Result == GameResult.BlackWin && myColor == Black))
            {
                newResult.Append(" You win!");
                if (CurrGame.DetailResult == GameDetailResult.WinByResign)
                    newResult.Append(" Opponent resigned.");
                else if (CurrGame.DetailResult == GameDetailResult.WinByTimeOver)
                    newResult.Append(" Time expired.");
                else if (CurrGame.DetailResult == GameDetailResult.WinByCheckmate)
                    newResult.Append(" Checkmate!");
            }

            else if ((CurrGame.Result == GameResult.WhiteWin && myColor == Black) ||
                     (CurrGame.Result == GameResult.BlackWin && myColor == White))
            {
                newResult.Append(" You lose.");
                if (CurrGame.DetailResult == GameDetailResult.WinByResign)
                    newResult.Append(" You resigned.");
                else if (CurrGame.DetailResult == GameDetailResult.WinByTimeOver )
                    newResult.Append(" Time expired.");
                else if (CurrGame.DetailResult == GameDetailResult.WinByCheckmate)
                    newResult.Append(" Checkmate.");
            }
            else if (CurrGame.Result == GameResult.Draw)
            {
                if (CurrGame.DetailResult == GameDetailResult.DrawByAgree)
                    newResult.Append("Opponents agreed to draw.");
                else if (CurrGame.DetailResult == GameDetailResult.DrawByStaleMate)
                    newResult.Append("Draw. Stalemate.");
                else if (CurrGame.DetailResult == GameDetailResult.DrawByTripleRepetition)
                    newResult.Append("Draw. Triple repetition.");
                else if (CurrGame.DetailResult == GameDetailResult.DrawByFiftyMoves)
                    newResult.Append("Draw. Fifty moves rule.");
            }

            string shortResult = GetGameShortResult(CurrGame.Result);
            newResult.Append("\n").Append(shortResult);
            SystemHelper.Instance.ShowMessage(newResult.ToString(), false);
            gameHeader.text += $"\n{shortResult}";
        }

        string GetGameShortResult(GameResult gameResult)
        {
            string shortRes;
            if (gameResult == GameResult.WhiteWin)
                shortRes = "1 - 0";
            else if (gameResult == GameResult.BlackWin)
                shortRes = "0 - 1";
            else if (gameResult == GameResult.Draw)
                shortRes = "1/2 - 1/2";
            else
                shortRes = "";

            return shortRes;
        }

        public void HandleMove(GameResponse gameResponse)
        {
            var inputMove = gameResponse.Move;
            if (inputMove.GameId != CurrGame.Id)
                return;

            if (inputMove.PlayerId != currPlayer.Id)
            {
                boardComponent.ShowPositionAfterMove(inputMove);
                SwapClock(inputMove.TimeInfo);
            }
                
            else
                SwapClock(inputMove.TimeInfo);
            
            CurrGame.Fen = inputMove.Fen;
            CurrGame.LastMove = inputMove.Value;
            CurrGame.TimeInfo = inputMove.TimeInfo;
        }
        private void SwapClock(TimeInfoDto timeInfo)
        {
            if (ChessClock.toMoveSide != toMoveSide)
                StartCoroutine(SwapClockImpl(timeInfo));
        }

        private IEnumerator SwapClockImpl(TimeInfoDto timeInfo)
        {
            ChessClock.Swap(timeInfo);
            yield break;
        }

        public void HandleDraw(GameResponse gameResponse)
        {
            DrawInfoDto drawInfo = gameResponse.DrawInfo;
            Debug.Log($"Draw {drawInfo.DrawFlag}");

            switch (drawInfo.DrawFlag)
            {
                case(DrawFlag.Offer):
                    if (drawInfo.FromPlayerId != currPlayer.Id)
                    {
                        confirmMenu.Show(new ConfirmData
                        {
                            Action = AcceptDraw,
                            CancelAction = DeclineDraw,
                            ActionSprite = Resources.Load<Sprite>("Buttons/Icon draw"),
                            ConfirmQuestion = $"{woodChess.CurrOpponent.Name} offer draw.\n Do you accept?"
                        });
                    }
                    break;
                case(DrawFlag.Decline):
                    SystemHelper.Instance.ShowDrawMessage(gameResponse.DrawInfo);
                    break;
            }
           
            ButtonsControl.SetDrawButtons(gameResponse.DrawInfo);
            CurrGame.DrawInfo = drawInfo;
        }

        private void AcceptDraw()
        {
            networkClient.AcceptDraw(CurrGame.Id);
        }

        private void DeclineDraw()
        {
            networkClient.DeclineDraw(CurrGame.Id);
        }

        public void HandleGames(GameResponse gameResponse)
        {
            throw new NotImplementedException();
        }

        public void ShowMessage(string message)
        {
            SystemHelper.Instance.ShowMessage(message, true);
        }

        public void HandleSystem(DataResponse dataResponse)
        {
            ShowMessage(dataResponse.Data);
        }

        public void HandleException(DataResponse dataResponse)
        {
            ShowMessage(dataResponse.Data);
        }

        public void HandleChat(DataResponse dataResponse)
        {
            throw new NotImplementedException();
        }

        public void HandleClock(DataResponse dataResponse)
        {
            woodChess.SynchronizeClockWs(dataResponse.ServerTime);
        }

        public void SendMove(string move)
        {
            if (ConfirmMenuObject)
                confirmMenu.Hide();
            boardComponent.ShowPositionAfterMove(move);
            NetworkClient.GetInstance().MakeMove(CurrGame.Id, move);
        }

        public bool IsValidPick(SideColor figureColor)
        {
            return IsGamePlaying() && figureColor == toMoveSide && IsMyMove();
        }

        private bool IsGamePlaying()
        {
            return CurrGame.Status == GameStatus.Play;
        }

        private void ToggleSettingsMenu()
        {
            GameObject.Find("MenuButtonsCanvas").GetComponent<Canvas>().enabled = settingsButton.IsOn;
        }
    }
}