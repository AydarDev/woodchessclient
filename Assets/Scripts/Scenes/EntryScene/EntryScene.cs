﻿using System;
using System.Linq;
using System.Text;
using Configuration;
using Globals;
using GO_Scripts;
using Helpers;
using NetworkServices;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using WebSocketModels;
using WebSocketModels.Models;
using WebSocketModels.UserManagerModels;
using static WebSocketModels.EncryptionHelper;

namespace Scenes.EntryScene
{
    public class EntryScene : MonoBehaviour
    {
        public GameObject loginCanvas;
        public GameObject registerCanvas;
        private WoodChess woodChess => WoodChess.GetInstance();
        private NetworkSettings networkSettings => woodChess.GameConfiguration.NetworkSettings;
        private SystemSettings systemSettings => woodChess.GameConfiguration.SystemSettings;
        private readonly NetworkClient networkClient = NetworkClient.GetInstance();
        string userDataFile;

        GameObject loginForm;
        GameObject registerForm;
        Transform mainCanvas;
        Text statusText;
        Credentials credentialsStore;

        private void Start()
        {
            try
            {
                woodChess.InitConfiguration();
                InitGameObjects();
                if ((systemSettings.LoginOnEnter))
                    TryLogin();
                else
                    ActivateLoginBox();
            }
            catch (Exception e)
            {
                ShowMessage(e.Message);
                ActivateLoginBox();
                Debug.LogError(e.StackTrace);
            }
        }


        private void OnServerDropDownChoose(int value)
        {
            SetGameServer(value);
        }

        private void SetGameServer(int value)
        {
            string serverName = networkSettings.GameServers[value];
            bool defaultPort = value == 0;
            networkSettings.ServerHost = serverName;
            networkSettings.GameServerPort = defaultPort ? "" : ":5000";
            networkSettings.PuzzleServerPort = defaultPort ? "" : ":8000";
        }

        private void Update()
        {
            try
            {
                MonitorEscapeAction();
            }
            catch (Exception e)
            {
                ShowMessage($"{e.Message}");
                Debug.LogError(e.StackTrace);
            }
        }

        private void InitGameObjects()
        {
            GameObject.Find("SceneCanvas");
            mainCanvas = GameObject.Find("MainCanvas").transform;
            statusText = GameObject.Find("StatusText").GetComponent<Text>();
        }

        private void TryLogin()
        {
            try
            {
                Credentials creds = ConfigurationHelper.ReadCredentials();
                if (creds != null)
                {
                    var model = CommonHelper.GetLoginModel(creds.Encrypt(networkKey));
                    WebClient.Instance.Login(model, HandleAuthReply);
                }
                else
                    ActivateLoginBox();
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
                statusText.text = e.Message;
                ActivateLoginBox();
            }
        }

        internal void HandleAuthReply(UserManagerResponse response)
        {
            if (response == null)
            {
                Debug.Log("Response not been received");
                ActivateLoginBox();
                return;
            }

            if (response.IsSuccess)
            {
                CheckAndSaveCredentials();
                ConfigurationHelper.SaveConfiguration();
                WoodChess.GetInstance().CurrPlayer = response.CurrPlayer;
                WoodChess.GetInstance().Token = response.Message;
                networkClient.PlayerId = response.CurrPlayer.Id;
                SceneManager.LoadScene("MainScene");
            }
            else
            {
                ShowUserManagerError(response);
                ActivateLoginBox();
            }
        }



        internal void HandleRegister(UserManagerResponse response)
        {
            if (response == null)
            {
                Debug.Log("Response not been received");
                return;
            }

            if (response.IsSuccess)
            {
                ShowMessage(response.Message);
                CheckAndSaveCredentials();
                TryLogin();
            }
            else
                ShowUserManagerError(response);
        }

        private void ShowUserManagerError(UserManagerResponse response)
        {
            StringBuilder errorMessage = new StringBuilder();
            errorMessage.Append(response.Message);
            int i = 1;
            if (response.Errors != null)
                foreach (string error in response.Errors)
                    errorMessage.Append($"\n{i++}. ").Append(error);

            ShowMessage(errorMessage.ToString());
        }

        public void ShowMessage(string result)
        {
            if (result == null) return;

            SystemHelper.Instance.ShowMessage(result, true);

            if (statusText)
            {
                if (result.Contains("Unauthorized") || result.Contains("Not Found"))
                    statusText.text = "Player Name or Password is wrong";
                else if ("Cannot resolve destination host".Equals(result))
                    statusText.text = "Connection problem.";
                else
                    statusText.text = result;
            }
        }

        private void MonitorEscapeAction()
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                Application.Quit();
            }
        }

        private void CheckAndSaveCredentials()
        {
            if (credentialsStore == null)
                return;
            ConfigurationHelper.WriteCredentials(credentialsStore);
            credentialsStore = null;
        }

        internal void StoreCredentials(Credentials credentials)
        {
            credentialsStore = new Credentials(credentials.UserName, credentials.Password);
        }

        public void ActivateLoginBox()
        {
            if (registerForm)
                Destroy(registerForm);

            if (loginForm)
                return;
            loginForm = Instantiate(loginCanvas, mainCanvas);
            loginForm.transform.SetAsFirstSibling();
        }

        public void RegisterPressed()
        {
            Destroy(loginForm);
            registerForm = Instantiate(registerCanvas, mainCanvas);
            registerForm.transform.SetAsFirstSibling();
        }
    }
}