﻿using System;
using Configuration;
using Globals;
using GO_Scripts;
using Helpers;
using NetworkServices;
using UnityEngine;
using UnityEngine.UI;
using WebSocketModels;
using WebSocketModels.Models;
using static WebSocketModels.EncryptionHelper;

namespace Scenes.EntryScene
{
    public class LoginForm : MonoBehaviour
    {
        GameObject mainCanvas => GameObject.Find("MainCanvas");
        private EntryScene entryScene => mainCanvas.GetComponent<EntryScene>();
        private Toggle rememberToggle => GameObject.Find("RememberToggle").GetComponent<Toggle>();
        NetworkClient networkClient => NetworkClient.GetInstance();
        private SystemSettings settings => WoodChess.GetInstance().GameConfiguration.SystemSettings;
        InputField loginInput;
        InputField passwordInput;
        Text statusText;

        private void Start()
        {
            InitGameObjects();
        }

        private void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
                Application.Quit();
        }


        private void InitGameObjects()
        {            
            //mainCanvas = GameObject.Find("MainCanvas");
            statusText = GameObject.Find("StatusText").GetComponent<Text>();
            loginInput = GameObject.Find("LoginInput").GetComponent<InputField>();
            passwordInput = GameObject.Find("PasswordInput").GetComponent<InputField>();

            GameObject.Find("LoginButton").GetComponent<ButtonScript>().action = LoginPressed;

            GameObject.Find("NewPlayerButton").GetComponent<ButtonScript>().action =
                entryScene.RegisterPressed;

            loginInput.Select();
            statusText.text = "";
            rememberToggle.SetIsOnWithoutNotify(WoodChess.GetInstance().GameConfiguration.SystemSettings.LoginOnEnter);
            rememberToggle.GetComponent<ToggleScript>().action = SetLoginOnEnter;
        }

        private void SetLoginOnEnter()
        {
            WoodChess.GetInstance().GameConfiguration.SystemSettings.LoginOnEnter = rememberToggle.isOn;
            ConfigurationHelper.SaveConfiguration();
        }


        public void LoginPressed()
        {
            Login();
        }

        private void Login()
        {
            try
            {
                statusText.text = "";
                if (loginInput.text == "" || passwordInput.text == "")
                {
                    statusText.text = "Please input \nLogin and Password";
                    return;
                }

                Credentials credentials = new Credentials(loginInput.text, passwordInput.text);

                entryScene.StoreCredentials(credentials);
                var loginModel = CommonHelper.GetLoginModel(credentials.Encrypt(networkKey));
                WebClient.Instance.Login(loginModel, entryScene.HandleAuthReply);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
                statusText.text = e.Message;
            }
        }
        
        private void ShowError(string result)
        {

            if (result.Contains("Unauthorized") || result.Contains("Not Found"))
                statusText.text = "Player Name or Password is wrong";

            else if ("Cannot resolve destination host".Equals(result))
                statusText.text = "Connection problem.";
        }
    }
}


