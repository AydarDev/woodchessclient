﻿using Configuration;
using Globals;
using GO_Scripts;
using Helpers;
using NetworkServices;
using UnityEngine;
using UnityEngine.UI;
using WebSocketModels;
using WebSocketModels.Models;

namespace Scenes.EntryScene
{
    public class RegisterForm : MonoBehaviour
    {
        GameObject mainCanvas;
        InputField nickNameInput;
        InputField eMailInput;
        InputField passwordInput;
        InputField passwordConfirmInput;
        Text statusText;
        private EntryScene entryScene => mainCanvas.GetComponent<EntryScene>();
        NetworkClient websocketClient => NetworkClient.GetInstance();
        
        private SystemSettings settings => WoodChess.GetInstance().GameConfiguration.SystemSettings;

        private void Start()
        {
            InitGameObjects();
        }

        private void InitGameObjects()
        {
            mainCanvas = GameObject.Find("MainCanvas");
            nickNameInput = GameObject.Find("NicknameInput").GetComponent<InputField>();
            eMailInput = GameObject.Find("EmailInput").GetComponent<InputField>();
            passwordInput = GameObject.Find("PwdInput").GetComponent<InputField>();
            passwordConfirmInput = GameObject.Find("PwdConfirmInput").GetComponent<InputField>();
            statusText = GameObject.Find("StatusText").GetComponent<Text>();

            GameObject.Find("SubmitButton").GetComponent<ButtonScript>().action = OnSubmitClick;

            nickNameInput.Select();
            statusText.text = "";
        }

        private void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
                mainCanvas.GetComponent<EntryScene>().ActivateLoginBox();
        }

        public void OnSubmitClick()
        {
            string playerName = nickNameInput.text;
            string email = eMailInput.text.Trim();
            string password = passwordInput.text;
            string passwordConfirm = passwordConfirmInput.text;

            if (!ValidationHelper.GetInstance().InputValidate(playerName, email, password, passwordConfirm))
            {
                ShowMessage(ValidationHelper.GetInstance().GetValidationMessage());
                return;
            }
            
            mainCanvas.GetComponent<EntryScene>().StoreCredentials(new Credentials(playerName, password));

            var model = CommonHelper.GetRegisterModel(playerName, email, password, passwordConfirm);
            WebClient.Instance.Register(model, entryScene.HandleRegister);
        }

        private void ShowMessage(string error)
        {
            statusText.text = error;
        }
    }
}
