﻿using WebSocketModels.WsReceivers;

namespace Scenes.MainScene
{
    interface IMainReceiver : IChallReceiver, IPlayerReceiver, IDataReceiver, IGameReceiver
    {
    }
}
