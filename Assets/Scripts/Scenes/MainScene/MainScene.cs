﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Globals;
using GO_Scripts;
using GO_Scripts.Toggles;
using Helpers;
using NetworkServices;
using Scenes.MainScene.PlayersRooms;
using Scenes.MainScene.PlayRoom;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using WebSocketModels.Enums;
using WebSocketModels.Models;
using WebSocketModels.WsMessages.WsResponses;
using static Helpers.AvatarHelper;
using Avatar = GO_Scripts.Avatar;

namespace Scenes.MainScene
{
    public class MainScene : MonoBehaviour, IMainReceiver
    {
        public GameObject playerAvatar;
        public GameObject menuItemsPrefab;
        public GameObject playRoomPrefab;
        public GameObject playersRoomPrefab;
        public GameObject inputChallengePrefab;
        public GameObject puzzleRoomPrefab;
        public GameObject myBoardPrefab;
        internal readonly List<DropDownHandler> currentDropdowns = new List<DropDownHandler>();
        PlayerDto currPlayer => woodChess.CurrPlayer;
        internal readonly List<ChallengeDto> privateOutputChallenges = new List<ChallengeDto>();

        private GameObject inputChallengeDialog;
        private GameObject menuItems => GameObject.Find("MenuItemsPanel");
        private GameObject playRoom;
        private GameObject playersRoom;
        private GameObject puzzleRoom;
        private GameObject myBoard;
        private Transform mainCanvas;
        private CustomToggle menuToggle;
        private Text currPlayerName;
        private Text currPlayerRating;
        private WebClient webClient => WebClient.Instance;
        private WoodChess  woodChess => WoodChess.GetInstance(); 

        private readonly Queue<ChallengeDto> inputChallenges = new Queue<ChallengeDto>();
        internal bool IsDialog;
        private bool _isClosing;
        private RatingType currGameType => woodChess.GameConfiguration.SystemSettings.CurrGameType;

        private readonly Dictionary<RatingType, string> ratingTypeIcons = new Dictionary<RatingType, string>
        {
            { RatingType.Bullet, "bullet" },
            { RatingType.Blitz, "lightning" },
            { RatingType.Rapid, "puma" },
            { RatingType.Classic, "chess-clock"},
            { RatingType.Daily, "calendar" },
            { RatingType.Puzzle, "puzzle" }
        };

        private const string Sprites = "Sprites";
        private Image ratingTypeIcon => GameObject.Find("RatingTypeIcon").GetComponent<Image>();

        private void Start()
        {
            InitGameObjects();
            InitWebSocketModules();
            SetButtonActions();
            SetPlayerInfo();
            OpenRoom(ref playRoom, playRoomPrefab);
            //OpenRoom(ref myBoard, myBoardPrefab, RatingType.Classic);
            GetCurrentGame();
        }

        private void GetCurrentGame()
        {
            webClient.GetCurrentGame(woodChess.CurrPlayer.Id, (game) => woodChess.CurrGame = game);
        }

        private void Update()
        {
            CheckInputChallenges();

            if (Input.GetKeyUp(KeyCode.Escape) && !_isClosing)
            {
                Debug.Log("Closing.");
                _isClosing = true;
                EscapeAction();
            }
        }

        private void EscapeAction()
        {
            if (IsDialog)
                return;

            if (menuToggle.IsOn)
            {
                menuToggle.ChangeValue();
                OnMenuPush();
            }

            else if (playRoom)
            {
                try
                {
                    WsClient.Instance.CloseConnection();
                }
                finally
                {
                    Application.Quit();
                }
            }

            else if (!playRoom)
                OpenRoom(ref playRoom, playRoomPrefab);
        }

        private void InitGameObjects()
        {
            mainCanvas = GameObject.Find("MainCanvas").transform;
            menuToggle = GameObject.Find("MenuToggle").GetComponent<CustomToggle>();
            currPlayerName = GameObject.Find("CurrPlayerName").GetComponent<Text>();
            currPlayerRating = GameObject.Find("CurrPlayerRating").GetComponent<Text>();
        }


        private void InitWebSocketModules()
        {
            WsClient.Instance.WsReceiver = this;
        }

        private void SetButtonActions()
        {
            menuToggle.action = OnMenuPush;
            menuItems.transform.Find("PlayRoomButton").GetComponent<ButtonScript>().action = OnPlayRoomPush;
            menuItems.transform.Find("PlayersButton").GetComponent<ButtonScript>().action = OnPlayersPush;
            menuItems.transform.Find("PuzzlesButton").GetComponent<ButtonScript>().action = OnPuzzlesPush;
            menuItems.transform.Find("LogoutButton").GetComponent<ButtonScript>().action = OnLogoutPush;
        }

        public void ShowMessage(string message)
        {
            SystemHelper.Instance.ShowMessage(message, true);
        }

        public void HandleChallenges(ChallengeResponse message)
        {
            if (playRoom)
                playRoom.GetComponent<ChallengesMonitor>().ShowChallenges(message.Challenges);
        }

        public void HandleChallenge(ChallengeResponse message)
        {
            if (!inputChallenges.Contains(message.Challenge))
                inputChallenges.Enqueue(message.Challenge);
        }

        public void HandleChallengeRemove(ChallengeResponse message)
        {
            ChallengeDto challenge = privateOutputChallenges.FirstOrDefault(
                ch => ch.Id.Equals(message.ChallengeId));

            if (challenge != null && challenge.FromPlayer.Equals(currPlayer))
            {
                privateOutputChallenges.Remove(challenge);
                ShowMessage("Challenge removed");
                playRoom.GetComponent<ChallengesMonitor>().UpdateChallenges();
            }

            else if (inputChallengeDialog &&
                     inputChallengeDialog.GetComponent<InputChallengeDialog>().challenge.Id.Equals(message.ChallengeId))
            {
                ShowMessage(
                    $"{inputChallengeDialog.GetComponent<InputChallengeDialog>().challenge.FromPlayer.Name}  withdrew a challenge.");
                Destroy(inputChallengeDialog);
            }
        }

        public void HandleChallengeAccept(ChallengeResponse message)
        {
            Debug.Log("ChallengeAccepted");
            woodChess.SynchronizeClockWs(message.ServerTime);
            ShowMessage($"Server time {message.ServerTime}");
            StartGame(message.Game);
        }

        public void HandleChallengeDecline(ChallengeResponse message)
        {
            ChallengeDto challenge = privateOutputChallenges.FirstOrDefault(
                ch => ch.Id.Equals(message.ChallengeId));

            if (challenge != null)
            {
                privateOutputChallenges.Remove(challenge);
                ShowMessage($"{challenge.ToPlayer.Name} declined your challenge.");

                if (playRoom)
                    playRoom.GetComponent<ChallengesMonitor>().UpdateChallenges();
            }
        }

        private void CheckInputChallenges()
        {
            if (inputChallenges.Count == 0 || IsDialog)
                return;

            ChallengeDto inputChallenge = inputChallenges.Dequeue();
            if (inputChallenge.ToPlayer.Id.Equals(currPlayer.Id))
            {
                StartInputChallengeDialog(inputChallenge);
                ShowMessage($"{inputChallenge.FromPlayer.Name} send a challenge to you.");
            }
            else if (inputChallenge.FromPlayer.Id.Equals(currPlayer.Id) &&
                     !privateOutputChallenges.Contains(inputChallenge))
                privateOutputChallenges.Add(inputChallenge);
        }

        private void StartGame(GameDto game)
        {
            try
            {
                if (game == null || game.Status != GameStatus.Play)
                    return;

                if (!game.WhitePlayer.Equals(currPlayer) && !game.BlackPlayer.Equals(currPlayer))
                    return;

                privateOutputChallenges.Clear();
                woodChess.CurrGame = game;
                ShowMessage("Trying to start game");
                SceneManager.LoadScene("Board");
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }
        
        private void OnPlayRoomPush()
        {
            OpenRoom(ref playRoom, playRoomPrefab);
            menuToggle.OnClick();
        }
        
        private void OnPlayersPush()
        {
            OpenRoom(ref playersRoom, playersRoomPrefab);
            menuToggle.OnClick();
        }
        
        private void OnPuzzlesPush()
        {
            OpenRoom(ref puzzleRoom, puzzleRoomPrefab, RatingType.Puzzle);
            menuToggle.OnClick();
        }
        
        private void OpenRoom(ref GameObject roomObject, GameObject roomPrefab, RatingType ratingType = RatingType.Blitz)
        {
            if(roomObject) return;
            if(mainCanvas.childCount > 0)
                Destroy(mainCanvas.GetChild(0).gameObject);
            roomObject = Instantiate(roomPrefab, mainCanvas);
            ShowCurrSpecificRating(ratingType);
        }

        private void SetPlayerInfo()
        {
            currPlayerName.text = currPlayer.Name;
            currPlayerRating.text = GetRatingText(currPlayer[currGameType]);
            playerAvatar.GetComponent<Avatar>().ViewAvatar(currPlayer);
        }

        private void OnMenuPush()
        {
            if (menuToggle.IsOn)
            {
                menuItems.GetComponent<Canvas>().enabled = true;
                CloseOtherDropdowns();
            }
            else
            {
                menuItems.GetComponent<Canvas>().enabled = false;
            }
        }

        private void CloseOtherDropdowns()
        {
            foreach (var dropdown in currentDropdowns)
                if (dropdown.isShow)
                    dropdown.OnMenuClick();
        }

        private void OnLogoutPush()
        {
            woodChess.ResetCurrGame();
            woodChess.ResetCurrPlayer();
            woodChess.GameConfiguration.SystemSettings.LoginOnEnter = false;
            ConfigurationHelper.SaveConfiguration();
            SceneManager.LoadScene("EntryScene");
        }
        

        internal void ShowCurrSpecificRating(RatingType ratingType)
        {
            currPlayerRating.text = GetRatingText(currPlayer[ratingType]);
            var ratingSprite = Resources.Load<Sprite>($"{Sprites}/{ratingTypeIcons[ratingType]}");
            ratingTypeIcon.sprite = ratingSprite;
        }

        internal void ShowRatingWithDeviation(RatingType ratingType, double deviation)
        {
            var builder = new StringBuilder();
            builder.Append(GetRatingText(currPlayer[ratingType]))
                .Append("(")
                .Append(deviation >= 0 ? "+" : "-")
                .Append(GetRatingText(deviation))
                .Append(")");

            currPlayerRating.text = builder.ToString();
        }


        private void StartInputChallengeDialog(ChallengeDto inputChallenge)
        {
            inputChallengeDialog = Instantiate(inputChallengePrefab, mainCanvas);
            inputChallengeDialog.transform.localPosition = new Vector2(0, 0);
            inputChallengeDialog.GetComponent<InputChallengeDialog>().SetChallenge(inputChallenge);

            GameObject menuBlocker = mainCanvas.transform.GetChild(0).transform.Find("MenuBlocker").gameObject;
            menuBlocker.GetComponent<Image>().enabled = true;
            IsDialog = true;
        }

        public void HandlePlayer(PlayerResponse playerResponse)
        {
            if (playersRoom)
                playersRoom.GetComponent<PlayersRoom>().HandlePlayer(playerResponse.Player);
        }
        public void HandleRatingDeviation(PlayerResponse playerResponse)
        {
            if (!puzzleRoom) return;
            puzzleRoom.GetComponent<PuzzleRoom.PuzzleRoom>().UpdatePlayerRating(playerResponse.RatingDeviation);
        }

        public void HandleSystem(DataResponse message)
        {
            ShowMessage(message.Data);
        }

        public void HandleException(DataResponse message)
        {
            Debug.LogError("Remote exception: " + message.Data);
            ShowMessage(message.Data);
        }
        public void HandleClock(DataResponse message)
        {
            woodChess.SynchronizeClockWs(message.ServerTime);
        }

        public void HandleGame(GameResponse gameResponse)
        {
            var game = gameResponse.Game;
            if ((game.WhitePlayer.Equals(woodChess.CurrPlayer) ||
                game.BlackPlayer.Equals(woodChess.CurrPlayer)) && game.Status == GameStatus.Play)
                woodChess.CurrGame = game;
        }

        public void HandleMove(GameResponse gameResponse)
        {
        }

        public void HandleDraw(GameResponse gameResponse)
        {
            Debug.Log($"Draw {gameResponse.DrawInfo.DrawFlag} in MainScene");
            ShowMessage($"Draw {gameResponse.DrawInfo.DrawFlag}");
        }

        private void OnDestroy()
        {
            WsClient.Instance.WsReceiver = null;
        }
    }
}