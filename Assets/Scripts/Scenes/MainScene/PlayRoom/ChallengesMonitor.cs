﻿using System.Collections.Generic;
using System.Linq;
using Globals;
using GO_Scripts;
using UnityEngine;
using WebSocketModels.Models;

namespace Scenes.MainScene.PlayRoom
{
    public class ChallengesMonitor : MonoBehaviour
    {
        public GameObject challengeButtonPrefab;
        public GameObject myChallengePanelPrefab;

        private WoodChess woodChess => WoodChess.GetInstance();
        private PlayRoom playRoom;
        private List<ChallengeDto> _challenges;
        List<ChallengeDto> privateOutputChallenges;

        private Transform challengesScreen;
        private Transform myChallengesScreen;
        private Transform avatars;

        private void Start()
        {
            InitGameObjects();
        }

        private void InitGameObjects()
        {
            playRoom = FindObjectsOfType<PlayRoom>().FirstOrDefault();
            privateOutputChallenges = GameObject.Find("SceneCanvas").GetComponent<MainScene>().privateOutputChallenges;
            challengesScreen = GameObject.Find("ChallengesCanvas").transform;
            myChallengesScreen = GameObject.Find("MyChallengesCanvas").transform;
        }
        internal void ShowChallenges(List<ChallengeDto> challenges)
        {
            _challenges = challenges;
            UpdateChallenges();
        }


        internal void UpdateChallenges()
        {
            IEnumerable<ChallengeDto> myChallenges = _challenges.Where(c => c.FromPlayer.Equals(woodChess.CurrPlayer)).
            Union(privateOutputChallenges);

            _challenges = _challenges.Except(myChallenges).ToList();

            RenewChallengesInfo(_challenges);
            RenewMyChallengesInfo(myChallenges);
        }


        private void RenewChallengesInfo(IEnumerable<ChallengeDto> challenges)
        {
            foreach (Transform challenge in challengesScreen)
                Destroy(challenge.gameObject);

            foreach (ChallengeDto challenge in challenges)
            {
                GameObject challengeButton = Instantiate(challengeButtonPrefab, challengesScreen);
                challengeButton.GetComponent<ButtonScript>().action = playRoom.OnAcceptChallengePush;

                DisplayChallenge(challenge, challengeButton);
            }
        }

        private void RenewMyChallengesInfo(IEnumerable<ChallengeDto> myChallenges)
        {
            foreach (Transform challenge in myChallengesScreen)
                Destroy(challenge.gameObject);

            foreach(ChallengeDto myChallenge in myChallenges)
            {
                GameObject myChallengePanel = Instantiate(myChallengePanelPrefab, myChallengesScreen);
                myChallengePanel.transform.Find("DeleteChallButton").GetComponent<ButtonScript>().action =
                    playRoom.OnRemoveChallengePush;

                DisplayChallenge(myChallenge, myChallengePanel);
            }
        }

        private void DisplayChallenge(ChallengeDto challenge, GameObject challengePanel)
        {
            challengePanel.GetComponent<ChallengeInfo>().challenge = challenge;
            challengePanel.GetComponent<ChallengeInfo>().ViewChallengeInfo();
        }

    }
}
