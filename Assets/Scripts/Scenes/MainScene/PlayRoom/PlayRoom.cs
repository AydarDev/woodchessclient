﻿using System.Collections.Generic;
using System.Linq;
using ChessRules;
using Configuration;
using Globals;
using GO_Scripts;
using NetworkServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using WebSocketModels;
using WebSocketModels.Enums;
using WebSocketModels.Models;

namespace Scenes.MainScene.PlayRoom
{
    public class PlayRoom : MonoBehaviour
    {
        public GameObject challengeDialogPrefab;
        private GameObject challengeDialogCanvas;
        private Transform playRoom;
        private Image menuBlocker;
        private WoodChess woodChess => WoodChess.GetInstance();
        private FavoriteChallenge favoriteChallenge => woodChess.GameConfiguration.FavoriteChallenge;
        private NetworkClient networkClient => NetworkClient.GetInstance();
        private ChallengesMonitor challengesMonitor;
        private GameObject sceneCanvas;
        private GameObject playButton;

        private void Start()
        {
            InitGameObjects();
            WebClient.Instance.GetChallenges(woodChess.CurrPlayer.Id, HandleChallenges);
        }

        private void InitGameObjects()
        {
            playRoom = this.transform;
            challengesMonitor = UnityEngine.Object.FindObjectsOfType<ChallengesMonitor>().FirstOrDefault();
            playButton = GameObject.Find("PlayButton");
            playButton.GetComponent<ButtonScript>().action = OnPlayButtonPush;
            playButton.transform.Find("PlaySetupButton").GetComponent<ButtonScript>().action = StartChallengeDialog;
            sceneCanvas = GameObject.Find("SceneCanvas");
            menuBlocker = this.transform.Find("MenuBlocker").GetComponent<Image>();
        }

        private void HandleChallenges(List<ChallengeDto> challenges)
        {
            //TODO: Handle private input/output challenges
            if (playRoom)
                playRoom.GetComponent<ChallengesMonitor>().ShowChallenges(challenges);
        }

        private void OnPlayButtonPush()
        {
            if (woodChess.CurrGame != null && !woodChess.CurrGame.IsNull && woodChess.CurrGame.Status == GameStatus.Play)
            {
                ResumeCurrGame();
                return;
            }
            TimeControlDto timeControl = new TimeControlDto(favoriteChallenge.TimeRange, favoriteChallenge.Increment);
            SideColor sideColor = favoriteChallenge.Color;

            networkClient.CreateChallenge(timeControl, sideColor);
        }

        private void ResumeCurrGame()
        {            
            StopAllCoroutines();
            SceneManager.LoadScene("Board");
        }

        public void OnAcceptChallengePush()
        {
            GameObject challButt = EventSystem.current.currentSelectedGameObject;

            var challengeId = challButt.GetComponent<ChallengeInfo>().challenge.Id;
            networkClient.AcceptChallenge(challengeId);
        }

        internal void OnRemoveChallengePush()
        {
            GameObject deleteButton = EventSystem.current.currentSelectedGameObject;
            var challengeId = deleteButton.transform.parent.GetComponent<ChallengeInfo>().challenge.Id;            
            networkClient.RemoveChallenge(challengeId);
        }


        private void StartChallengeDialog()
        {
            challengeDialogCanvas = Instantiate(challengeDialogPrefab, playRoom);
            challengeDialogCanvas.transform.localPosition = new Vector3(0, -300, 0);
            challengeDialogCanvas.GetComponent<CreateChallengeDialog>().SetPlayerData(null);
            menuBlocker.enabled = true;
            GameObject.Find("SceneCanvas").GetComponent<MainScene>().IsDialog = true;
        }
    }
}
