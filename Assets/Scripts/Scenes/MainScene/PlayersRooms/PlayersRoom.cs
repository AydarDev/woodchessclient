﻿using System.Collections.Generic;
using System.Linq;
using Globals;
using GO_Scripts;
using NetworkServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using WebSocketModels.Enums;
using WebSocketModels.Models;
using WebSocketModels.Models.NullClasses;

namespace Scenes.MainScene.PlayersRooms
{
    public class PlayersRoom : MonoBehaviour
    {
        public GameObject playerPanelPrefab;
        public GameObject challengePanelPrefab;

        private GameObject challengeCanvas;
        private Transform sceneCanvas;
        private Transform mainCanvas;
        private MainScene mainScene;
        Transform playersPanel;
        private Dropdown filterDropDown;
        private WebClient webClient => WebClient.Instance;
        private WoodChess woodChess => WoodChess.GetInstance();
        private RatingType currGameType => woodChess.GameConfiguration.SystemSettings.CurrGameType;
        private readonly Dictionary<long, PlayerData> playerInfos = new Dictionary<long, PlayerData>();

        private void Start()
        {
            InitGameObjects();
            GetOnlinePlayers();
        }

        private void OnDestroy()
        {
            mainScene.currentDropdowns.Remove(filterDropDown.GetComponent<DropDownHandler>());
        }

        private void InitGameObjects()
        {
            sceneCanvas = GameObject.Find("SceneCanvas").transform;
            mainCanvas = GameObject.Find("MainCanvas").transform;
            challengeCanvas = GameObject.Find("ChallengeCanvas");
            GameObject.Find("PlayersCanvas");
            filterDropDown = GameObject.Find("FilterDropdown").GetComponent<Dropdown>();
            filterDropDown.GetComponent<DropDownHandler>().action = OnFilterDropDownChange;
            playersPanel = GameObject.Find("PlayersPanel").transform;
            mainScene = sceneCanvas.GetComponent<MainScene>();
            mainScene.currentDropdowns.Add(filterDropDown.GetComponent<DropDownHandler>());
        }

        private void GetOnlinePlayers()
        {
            webClient.GetOnlinePlayers(HandlePlayers);
        }

        private void GetAllPlayers()
        {
            webClient.GetAllPlayers(HandlePlayers);
        }
        private void HandlePlayers(List<PlayerDto> players)
        {
            ViewPlayersInfo(players);
        }

        internal void HandlePlayer(PlayerDto player)
        {
            if (filterDropDown.value == (int)FilterDropDownValues.OnlinePlayers)
                ViewPlayersInfo(player);
            else if (filterDropDown.value == (int)FilterDropDownValues.AllPlayers) 
                UpdateOnlineStatus(player);
        }

        private void UpdateOnlineStatus(PlayerDto player)
        {
            var playerPanel = playerInfos[player.Id];
            var currPlayer = playerPanel.GetComponent<PlayerData>();
            currPlayer.player = player;
            currPlayer.UpdatePlayerStatus();
        }
        private void ViewPlayersInfo(IEnumerable<PlayerDto> players)
        {
            foreach (Transform child in playersPanel) Destroy(child.gameObject);
            
            playerInfos.Clear();
            foreach (var player in players.OrderByDescending(p => p[currGameType])) 
                CreatePlayerPanel(player);
        }

        private void ViewPlayersInfo(PlayerDto newPlayer)
        {
            foreach (Transform child in playersPanel) Destroy(child.gameObject);
            var players = playerInfos.Values
                .Where(p => p.player.Id != newPlayer.Id)
                .Select(p => p.player).ToList();
            
            if(newPlayer.IsOnline)
                players.Add(newPlayer);
            
            playerInfos.Clear();
            foreach (var player in players.OrderByDescending(p => p[currGameType]))
                CreatePlayerPanel(player);
        }

        private void CreatePlayerPanel(PlayerDto player)
        {
            var playerPanel = Instantiate(playerPanelPrefab, playersPanel);
            playerPanel.transform.localScale = new Vector3(1, 1, 0);
            var playerData = playerPanel.GetComponent<PlayerData>();
            playerData.player = player;
            playerPanel.transform.Find("PlayerDropdown").GetComponent<DropDownHandler>().action = OnPlayerDropdownChange;
            playerData.ViewPlayerInfo();
            playerInfos.Add(player.Id, playerData);
        }
        

        private void OnFilterDropDownChange(int dropDownValue)
        {            
            if (dropDownValue == 0)
                GetOnlinePlayers();
            else if (dropDownValue == 1)
                GetAllPlayers();
        }


        private void OnPlayerDropdownChange(int value)
        {            
            Dropdown playerDropdown = EventSystem.current.currentSelectedGameObject.GetComponent<Dropdown>();

            switch (value)
            {
                case(0):
                    PlayerDto player = playerDropdown.gameObject.transform.parent.GetComponent<PlayerData>().player;
                    StartChallengeSendDialog(player);
                    break;
            }
        }

        private void StartChallengeSendDialog(PlayerDto player)
        {
            challengeCanvas = Instantiate(challengePanelPrefab, mainCanvas);
            challengeCanvas.GetComponent<CreateChallengeDialog>().SetPlayerData(player);
            GameObject.Find("SceneCanvas").GetComponent<MainScene>().IsDialog = true;
        }
    }
}
