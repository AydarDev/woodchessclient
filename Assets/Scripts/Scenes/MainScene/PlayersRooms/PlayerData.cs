﻿using System.Text;
using UnityEngine;
using UnityEngine.UI;
using WebSocketModels.Enums;
using WebSocketModels.Models;
using static Helpers.AvatarHelper;
using Avatar = GO_Scripts.Avatar;

namespace Scenes.MainScene.PlayersRooms
{
    public class PlayerData : MonoBehaviour
    {
        public PlayerDto player;
        public GameObject playerAvatar;
        public GameObject playerInfo;
        
        public void ViewPlayerInfo()
        {
            playerAvatar.GetComponent<Avatar>().ViewAvatar(player);
            playerAvatar.GetComponent<Avatar>().ViewPlayerStatus(player);
            ViewPlayerData();
        }

        public void UpdatePlayerStatus()
        {
            playerAvatar.GetComponent<Avatar>().ViewPlayerStatus(player);
        }
        
        private void ViewPlayerData()
        {
            StringBuilder playerData = new StringBuilder();
            playerData.Append(player.Name)
                .Append(" (")
                .Append(GetRatingText(player[RatingType.Blitz]))
                .Append(")");

            playerInfo.GetComponent<Text>().text = playerData.ToString();
        }
    }
}
