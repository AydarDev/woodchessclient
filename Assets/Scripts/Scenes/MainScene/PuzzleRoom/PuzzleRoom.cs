﻿using System;
using System.Collections;
using System.Collections.Generic;
//using System.IO;
using Boards;
using ChessRules;
using Configuration;
using GameInstruments;
using GameInstruments.Models;
using GameInstruments.NotationBuilders;
using Globals;
using GO_Scripts;
using Helpers;
using NetworkServices;
using UnityEngine;
using UnityEngine.UI;
using WebSocketModels.Boards;
using static Helpers.CommonHelper;
using WebSocketModels.Enums;
using WebSocketModels.Models.Puzzles;
using WebSocketModels.Models.Ratings;
using PuzzleType = WebSocketModels.Enums.PuzzleType;

namespace Scenes.MainScene.PuzzleRoom
{
    public class PuzzleRoom : MonoBehaviour, IBoardConsumer
    {
        private BoardComponent boardComponent => GameObject.Find("Board").GetComponent<BoardComponent>();
        private PuzzleEngine puzzleEngine;
        private MainScene mainScene;
        private PuzzleDto currPuzzle;
        private GameInfo currGameInfo;
        private GameObject nextButton => GameObject.Find("NextButton");
        private Canvas nextButtonCanvas => GameObject.Find("NextButtonCanvas").GetComponent<Canvas>();
        private GameObject puzzleTypeDropDown;
        private readonly WoodChess woodChess = WoodChess.GetInstance();
        private SystemSettings systemSettings => woodChess.GameConfiguration.SystemSettings;
        private SideColor playerColor => currGameInfo.PlayerColor;
        private UnityBoard board => boardComponent.board;
        private Ply responsePly = NullPly.GetInstance();
        private bool isResolved;
        private bool isReplay;
        private readonly SystemHelper systemHelper = SystemHelper.Instance;

        private WebClient webClient => WebClient.Instance;
        private static string _puzzleFile;

        private readonly BaseNotationBuilder notationBuilder = new NotationBuilder();

        private readonly Dictionary<SideColor, string> toMoveText = new Dictionary<SideColor, string>()
        {
            { SideColor.White, "White to move." },
            { SideColor.Black, "Black to move." }
        };

        private Text puzzleNotation;
        private Text ToMoveInfo => GameObject.Find("ToMoveInfo").GetComponent<Text>();
        private Text PuzzleCaption => GameObject.Find("PuzzleCaption").GetComponent<Text>();

        private void Start()
        {
            board.BoardConsumer = this;
            InitGameObjects();
            puzzleEngine = new PuzzleEngine();
            LoadNextPuzzle();
        }

        public bool IsMyMove()
        {
            return true;
        }

        public bool IsValidPick(SideColor figureColor)
        {
            return figureColor == playerColor && !isResolved;
        }

        public void Rotate()
        {
            //doNothing
        }


        private void OnDestroy()
        {
            if (GameObject.Find("NextButtonCanvas"))
                nextButtonCanvas.enabled = false;
            mainScene.currentDropdowns.Remove(puzzleTypeDropDown.GetComponent<DropDownHandler>());
        }

        private void InitGameObjects()
        {
            puzzleNotation = GameObject.Find("PuzzleNotation").GetComponent<Text>();
            mainScene = GameObject.Find("SceneCanvas").GetComponent<MainScene>();
            puzzleTypeDropDown = GameObject.Find("PuzzleTypeDropdown");
            puzzleTypeDropDown.GetComponent<DropDownHandler>().action = ChoosePuzzleType;
            puzzleTypeDropDown.GetComponent<DropDownHandler>().SetValueWithoutNotify((int)systemSettings.CurrPuzzleType);
            mainScene.currentDropdowns.Add(puzzleTypeDropDown.GetComponent<DropDownHandler>());
            nextButton.GetComponent<ButtonScript>().action = LoadNextPuzzle;
            GameObject.Find("ReplayButton").GetComponent<ButtonScript>().action = ReloadPuzzle;
            GameObject.Find("SecretButton").GetComponent<ButtonScript>().action = GetCsvPuzzle;
        }

        private void GetCsvPuzzle()
        {
            Debug.Log("GetCsv click!");
            webClient.GetCsvPuzzle(HandleGetNextPuzzleResult);
        }

        private void LoadNextPuzzle()
        {
            nextButtonCanvas.enabled = false;
            systemHelper.ShowMessage("Loading Puzzle");
            if (woodChess.CurrentPuzzles.TryGetValue(systemSettings.CurrPuzzleType, out var puzzle) && !isResolved)
                StartCoroutine(LoadPuzzle(puzzle));
            else
                GetNextPuzzleFromWeb(systemSettings.CurrPuzzleType);
        }

        private void GetNextPuzzleFromWeb(PuzzleType type)
        {
            webClient.GetNextPuzzle(type, HandleGetNextPuzzleResult);
        }

        /*private void GetNextPuzzleFromFile(string file)
        {
            string json = File.ReadAllText(file);
            var puzzle = JsonConvert.DeserializeObject<PuzzleDto>(json);
            HandleGetNextPuzzleResult(puzzle);
        }*/

        private void HandleGetNextPuzzleResult(PuzzleDto receivedPuzzle)
        {
            try
            {
                if(receivedPuzzle == null || receivedPuzzle.IsNull)
                {
                    systemHelper.ShowMessage("Cannot get next puzzle!");
                    return;
                }
                isReplay = false;
                if (systemSettings.DevMode)
                    mainScene.ShowMessage($"Received puzzle {receivedPuzzle}");
                if (!receivedPuzzle.Equals(currPuzzle))
                {
                    StartCoroutine(LoadPuzzle(receivedPuzzle));
                    woodChess.CurrentPuzzles[receivedPuzzle.PuzzleType] = receivedPuzzle;
                }

                else
                    Debug.Log("Puzzle is the same, not loading");
            }
            catch (Exception e)
            {
                if (systemSettings.DevMode)
                    mainScene.ShowMessage(e.Message);
                throw;
            }
        }

        private void ChoosePuzzleType(int selectedValue)
        {
            var selectedPuzzleType = (PuzzleType)selectedValue;
            systemSettings.CurrPuzzleType = selectedPuzzleType;
            ConfigurationHelper.SaveConfiguration();
            if (woodChess.CurrentPuzzles.TryGetValue(selectedPuzzleType, out var puzzle))
                StartCoroutine(LoadPuzzle(puzzle));
            else
                GetNextPuzzleFromWeb(selectedPuzzleType);
        }

        private void ShowPuzzleCaptions()
        {
            PuzzleCaption.text = currPuzzle.Name;
        }

        private void ShowToMoveInfo()
        {
            toMoveText.TryGetValue(playerColor, out var playerColorText);
            ToMoveInfo.text = playerColorText ?? "No to move info...";
        }

        public void SendMove(string move)
        {
            StartCoroutine(HandleMoveAsync(move));
        }

        private IEnumerator HandleMoveAsync(string move)
        {
            GuessMoveResponse response = puzzleEngine.GuessMove(move);
            mainScene.ShowMessage(response.Message);
            responsePly = response.ResponsePly;
            if (response.Result != ResultType.Fail)
                ShowGameNotation(response.GuessPly);

            switch (response.Result)
            {
                case ResultType.LineSolved:
                {
                    var basePly = response.ResponsePly.PreviousPly;
                    boardComponent.ShowPositionAfterMove(move);
                    yield return TwoSeconds;
                    board.GoToPosition(basePly.Fen, basePly.Move);
                    yield return OneSecond;
                    ShowGameNotation(responsePly);
                    boardComponent.ShowPositionAfterMove(response.ResponsePly.Move);
                    yield break;
                }
                case ResultType.PuzzleSolved:
                {
                    boardComponent.ShowPositionAfterMove(move);
                    ResolvePuzzle();
                    
                    yield break;
                }
                case ResultType.Fail:
                {
                    boardComponent.ResetFigurePosition();
                    Debug.Log("Wrong Move!");
                    yield break;
                }
            }

            boardComponent.ShowPositionAfterMove(move);

            yield return new WaitForSeconds(1f);
            ShowGameNotation(responsePly);
            boardComponent.ShowPositionAfterMove(response.Move);
        }

        internal void UpdatePlayerRating(RatingDeviationDto ratingDeviation)
        {
            mainScene.ShowRatingWithDeviation(RatingType.Puzzle, ratingDeviation.Deviation);
            woodChess.CurrPlayer.SetRating(RatingType.Puzzle, ratingDeviation.NewRating);
        }

        private void ShowNextPuzzleButton()
        {
            nextButtonCanvas.enabled = true;
        }

        private void ResolvePuzzle()
        {
            woodChess.CurrentPuzzles.Remove(currPuzzle.PuzzleType);
            isResolved = true;
            ShowNextPuzzleButton();

            if (!isReplay) CheckSolutionInWeb();
        }

        private void CheckSolutionInWeb()
        {
            int solutionKey = puzzleEngine.GetSolutionKey(currGameInfo);
            webClient.FinishPuzzle(CommonHelper.GetPuzzleFinishModel(currPuzzle, solutionKey, true));
        }

        /*private void OnPuzzleResolve(string json)
        {
            Debug.Log($"Received json {json}");
            RatingDeviationDto ratingDeviation = JsonConvert.DeserializeObject<RatingDeviationDto>(json);
            UpdatePlayerRating(ratingDeviation);

            woodChess.CurrentPuzzles.Remove(currPuzzle.PuzzleType);
        }*/

        private IEnumerator LoadPuzzle(PuzzleDto receivedPuzzle)
        {
            isResolved = false;
            currPuzzle = receivedPuzzle;
            currGameInfo = puzzleEngine.LoadPuzzle(currPuzzle);
            board.SetUpBoard(puzzleEngine.InitialFen, playerColor);
            nextButtonCanvas.enabled = false;
            puzzleNotation.text = string.Empty;
            mainScene.ShowMessage(string.Empty);
            mainScene.ShowCurrSpecificRating(RatingType.Puzzle);
            ShowPuzzleCaptions();
            if (currPuzzle.HasPreMove)
            {
                yield return new WaitForSeconds(1);
                ShowGameNotation(currGameInfo.PreMove);
                boardComponent.ShowPositionAfterMove(currGameInfo.PreMove.Move);
            }

            ShowToMoveInfo();
        }

        private void ReloadPuzzle()
        {
            isReplay = true;
            Debug.Log($"Trying to reload puzzle {currPuzzle.Name}");
            StartCoroutine(LoadPuzzle(currPuzzle));
        }

        private void ShowGameNotation(Ply ply /*bool includeLastMove = true*/)
        {
            var notation = puzzleNotation.text;
            var fen = board.Chess.Fen;
            puzzleNotation.text = notationBuilder.BuildNotation(notation,
                NotationTranslator.GetShortNotation(ply.Move, fen), ply.PlyNum);
        }
    }
}