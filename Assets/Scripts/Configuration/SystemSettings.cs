﻿using WebSocketModels.Enums;

namespace Configuration
{
    public class SystemSettings
    {
        public bool DevMode;
        public bool LoginOnEnter = true;
        public PuzzleType CurrPuzzleType = PuzzleType.Tactics;
        public RatingType CurrGameType = RatingType.Blitz;
    }
}