﻿using System.Collections.Generic;
using ChessRules;
using WebSocketModels.Enums;

namespace Configuration
{
    public class GameConfiguration
    {
        public SystemSettings SystemSettings { get; set; }
        public NetworkSettings NetworkSettings { get; set; }
        public BoardSettings BoardSettings { get; set; }
        public FavoriteChallenge FavoriteChallenge { get; set; }

        public static GameConfiguration GetDefault()
        {
            return new GameConfiguration
            {
                SystemSettings = new SystemSettings(),
                NetworkSettings = new NetworkSettings
                {
                    ServerHost = "ec2-51-20-63-40.eu-north-1.compute.amazonaws.com",
                    Protocol = "http",
                    GameServerPort = "5000"
                },
                BoardSettings = new BoardSettings(),
                FavoriteChallenge = new FavoriteChallenge
                {
                    Color = SideColor.None,
                    TimeRange = 5,
                    Increment = 3,
                    ratingType = RatingType.Blitz
                }
            };
        }
    }
}