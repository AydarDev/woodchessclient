﻿using ChessRules;
using WebSocketModels.Enums;

namespace Configuration
{
    public class FavoriteChallenge
    {
        public SideColor Color;
        public int TimeRange;
        public int Increment;
        public RatingType ratingType;
    }
}