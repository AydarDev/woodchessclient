﻿using System;
using System.Collections.Generic;

namespace Configuration
{
    public class NetworkSettings
    {
        
        public Dictionary<int, string> GameServers;
        public string ServerHost = "ec2-51-20-63-40.eu-north-1.compute.amazonaws.com";
        public string GameServerPort = "5000";
        public string PuzzleServerPort;
        public string Protocol;
        private string woodChessSepr => Protocol != String.Empty ? ":" : "";
        private string puzzlesSepr => Protocol != String.Empty ? ":" : "";

        private const string Http = "http";
        private const string Https = "https";
        private const string Ws = "ws";
        private const string Wss = "wss";

        private readonly Dictionary<string, string> webSocketProtocols = new Dictionary<string, string>()
        {
            {Http, Ws}, {Https, Wss}
        };

        internal string WebSocketServerAddress => $"{webSocketProtocols[Protocol]}://{ServerHost}{woodChessSepr}{GameServerPort}/chess_api/ws";
        internal string GameWebServerAddress => $"{Protocol}://{ServerHost}{woodChessSepr}{GameServerPort}";
        internal string PuzzleWebServerAddress => $"{Protocol}://{ServerHost}{puzzlesSepr}{PuzzleServerPort}";
    }
}