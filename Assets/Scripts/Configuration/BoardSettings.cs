﻿namespace Configuration
{
    public class BoardSettings
    {
        public bool IsMarkLastMove { get; set; } = true;
        public bool IsMarkPossibleMoves { get; set; } = true;
        public int AnimationSpeed { get; set; } = 40;
        public bool AutoPromotionToQueen { get; set; } = false;
    }
}