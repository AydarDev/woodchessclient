﻿using System;
using System.Text.RegularExpressions;

namespace Helpers
{
    class ValidationHelper
    {
        private static ValidationHelper validationHelper;

        private String validationMessage = "";

        private const string eMailPattern = @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$";

        internal static ValidationHelper GetInstance()
        {
            if (validationHelper == null)
                validationHelper = new ValidationHelper();

            return validationHelper;
        }

        private ValidationHelper() { }

        internal string GetValidationMessage()
        {
            return validationMessage;
        }

        internal bool InputValidate(string nickname, string email, string password, string passwordConfirm)
        {
            if (!ValidateNickname(nickname)) 
                return false;
            else if (!ValidateEmail(email)) 
                return false;
            else if (!ValidatePassword(password, passwordConfirm)) 
                return false;
            else 
                return true;
        }


        private bool ValidateNickname(string nickname)
        {
            if ("".Equals(nickname))
            {
                validationMessage = "Please input your nick!";
                return false;
            }
            else if (nickname.Length > 15)
            {
                validationMessage = "Nickname must be less than 15 symbols";
                return false;
            }
            else if (nickname.Length < 3)
            {
                validationMessage = "Nickname must be more than 2 symbols";
                return false;
            }
            else
                return true;
        }

        internal bool ValidateEmail(string email)
        {
            if ("".Equals(email))
            {
                validationMessage = "Please input your e-mail.";
                return false;
            }
            else if (!Regex.IsMatch(email, eMailPattern, RegexOptions.IgnoreCase))
            {
                validationMessage = "Incorrect e-mail format";
                return false;
            }
            else
                return true;
        }

        private bool ValidatePassword(string password, string passwordConfirm)
        {
            if ("".Equals(password))
            {
                validationMessage = "Please input your password.";
                return false;
            }
            else if (password.Length < 4)
            {
                validationMessage = "Password must be at least 4 symbols";
                return false;
            }
            else if ("".Equals(passwordConfirm))
            {
                validationMessage = "Please confirm your password.";
                return false;
            }
            else if (!password.Equals(passwordConfirm))
            {
                validationMessage = "Passwords are not the same.";
                return false;
            }
            else
                return true;
        }

    }
}
