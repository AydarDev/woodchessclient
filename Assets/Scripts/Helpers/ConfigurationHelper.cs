﻿using System;
using System.IO;
using Configuration;
using Globals;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using WebSocketModels.Models;
using static WebSocketModels.EncryptionHelper;

// ReSharper disable ConvertToUsingDeclaration

namespace Helpers
{
    public static class ConfigurationHelper
    {
        private const string ConfigFile = "/config.json";
        private static string configFileName => Application.persistentDataPath + ConfigFile;
        private const string GameConfiguration = "GameConfiguration";
        private const string Credentials = "Credentials";

        private static JObject ReadConfigJsonFromFile()
        {
            try
            {
                using (StreamReader file = File.OpenText(configFileName))
                using (JsonTextReader reader = new JsonTextReader(file))
                {
                    JObject config = (JObject) JToken.ReadFrom(reader);
                    return config;
                }
            }
            catch (JsonReaderException e)
            {
                Console.WriteLine(e);
                return new JObject();
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e);
                return new JObject();
            }
        }


        private static void WriteConfigJsonToFile(JObject entry)
        {
            using (StreamWriter file = File.CreateText(configFileName))
            using (JsonTextWriter writer = new JsonTextWriter(file))
                entry.WriteTo(writer);
        }

        internal static void WriteCredentials(Credentials credentialsToWrite)
        {
            JObject credentials = JObject.FromObject(credentialsToWrite.Encrypt(internalKey));

            JObject currConfig = ReadConfigJsonFromFile();
            currConfig[Credentials] = credentials;

            WriteConfigJsonToFile(currConfig);
        }

        internal static Credentials ReadCredentials()
        {
            try
            {
                JObject currentConfig = ReadConfigJsonFromFile();
                JObject credentialsObj = (JObject) currentConfig[Credentials];
                if (credentialsObj != null)
                {
                    Credentials credentials = credentialsObj.ToObject<Credentials>();
                    var decryptedCredentials = credentials.Decrypt(internalKey);
                    return decryptedCredentials;
                }

                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }
        
        internal static GameConfiguration LoadConfiguration()
        {
            try
            {
                JObject currentConfig = ReadConfigJsonFromFile();
                JObject credentialsObj = (JObject) currentConfig[GameConfiguration];
                if (credentialsObj != null)
                    return credentialsObj.ToObject<GameConfiguration>();

                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        internal static void SaveConfiguration()
        {
            JObject configuration = JObject.FromObject(WoodChess.GetInstance().GameConfiguration);
            JObject currConfig = ReadConfigJsonFromFile();
            if (configuration != null)
                currConfig[GameConfiguration] = configuration;
            WriteConfigJsonToFile(currConfig);
        }
    }
}