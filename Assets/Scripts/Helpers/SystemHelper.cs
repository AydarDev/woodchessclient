﻿using System;
using System.Collections;
using System.Text;
using Globals;
using UnityEngine;
using UnityEngine.UI;
using WebSocketModels.Enums;
using WebSocketModels.Models;

namespace Helpers
{
    public class SystemHelper : MonoBehaviour
    {
        private GameObject MessageScreen => GameObject.Find("MessageScreen");
        private Text statusText => MessageScreen.GetComponent<Text>();
        
        public static SystemHelper Instance;
        private void Start()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else if (Instance == this)
                Destroy(gameObject);

            DontDestroyOnLoad(gameObject);
        }
        
        public void ShowMessage(string message, bool disappear = true, bool withLog = false)
        {
            StartCoroutine(ShowSystemMessage(message, disappear, withLog));
        }


        private IEnumerator ShowSystemMessage(string inputSystemMessage, bool disappear, bool withLog = false)
        {
            statusText.text = inputSystemMessage;
            if (disappear)
            {
                yield return (CommonHelper.ThreeSeconds);
                statusText.text = "";
            }

            if (withLog) Log(inputSystemMessage);
        }

        public void ShowDrawMessage(DrawInfoDto drawInfo)
        {
            StringBuilder message = new StringBuilder();
            if (drawInfo.DrawFlag == DrawFlag.None) return;
            message.Append(drawInfo.FromPlayerId == WoodChess.GetInstance().CurrPlayer.Id
                ? "You "
                : $"{WoodChess.GetInstance().CurrOpponent.Name} ");
            
            switch (drawInfo.DrawFlag)
            {
                case DrawFlag.Offer:
                    message.Append("offered a draw");
                    break;
                case DrawFlag.Decline:
                    message.Append("declined a draw");
                    break;
                case DrawFlag.None:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            ShowMessage(message.ToString(), true);
        }
        
        internal void Log(string message)
        {
            Debug.Log(message);
        }
    }
}