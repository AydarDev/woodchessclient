﻿using UnityEngine;
using UnityEngine.UI;

namespace Helpers
{
    public class ScreenHelper : MonoBehaviour
    {
        private GameObject sceneCanvas;

        private void Start()
        {
            sceneCanvas = GameObject.Find("SceneCanvas");
            ScreenCalibrate();
        }

        private void ScreenCalibrate()
        {
            Camera camera = GameObject.Find("Main Camera").GetComponent<Camera>();
            camera.transform.position = new Vector2(0, 0);
            float ResX = camera.pixelWidth;
            float ResY = camera.pixelHeight;

            sceneCanvas.GetComponent<CanvasScaler>().referenceResolution = new Vector2(ResX, ResY);

            float height = 0;
            foreach (Transform child in sceneCanvas.transform)
                height += child.gameObject.GetComponent<RectTransform>().sizeDelta.y;

            float HeightScaleKoef = ResY / height;
            float WidthScaleKoef = ResX / 900;

            float scaleKoef;
            float cameraRatioCoef;

            if (WidthScaleKoef <= HeightScaleKoef)
            {
                scaleKoef = WidthScaleKoef;
                cameraRatioCoef = (float)(ResX / 9.125);
                camera.orthographicSize = camera.pixelHeight / cameraRatioCoef;
            }

            else
            {
                scaleKoef = HeightScaleKoef;
                cameraRatioCoef = (float)(900 * scaleKoef / 9.125);
                camera.orthographicSize = camera.pixelHeight / cameraRatioCoef;
            }


            foreach (Transform child in sceneCanvas.transform)
                child.localScale = new Vector2(scaleKoef, scaleKoef);
        }
    }
}
