﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using ChessRules;
using WebSocketModels.Enums;
using WebSocketModels.Models;

namespace Helpers
{
    static class AvatarHelper
    {

        private static readonly Dictionary<string, string> BaseFigureNames = new Dictionary<string, string>()
        {
            {"P", "Pawn"},
            {"N", "Knight"},
            {"B", "Bishop"},
            {"R", "Rook"},
            {"Q", "Queen"},
            {"K", "King"}
        };

        private static readonly Dictionary<SideColor, string> ColorNames = new Dictionary<SideColor, string>()
        {
            {SideColor.White, "White"},
            {SideColor.Black, "Black"},
            {SideColor.None, "Random"}
        };

        internal static string GetAvatarResource(PlayerDto player, RatingType ratingType, SideColor color = SideColor.White)
        {
            StringBuilder playerString = new StringBuilder();

            var playerFigure = GetPlayerFigure(player[ratingType]);
            playerString.Append("Figures/Avatars/")
                .Append(ColorNames[color])
                .Append(BaseFigureNames[playerFigure]);

            return playerString.ToString();
        }

        private static string GetPlayerFigure(double rating)
        {
            if (rating < 1500)
                return "P";
            if (rating >= 1500 & rating < 1600)
                return "N";
            if (rating >= 1600 & rating < 1800)
                return "B";
            if (rating >= 1800 & rating < 2000)
                return "R";
            if (rating >= 2000)
                return "Q";
            return "";
        }

        public static string GetRatingText(double rating)
        {
            return Math.Round(rating, MidpointRounding.AwayFromZero).ToString(CultureInfo.InvariantCulture);
        }
    }
}
