﻿using System;
using Globals;
using UnityEngine;
using WebSocketModels.Models;
using WebSocketModels.Models.Puzzles;
using WebSocketModels.UserManagerModels;
using static WebSocketModels.EncryptionHelper;

namespace Helpers
{
    internal static class CommonHelper
    {
        internal static WaitForSeconds OneSecond = new WaitForSeconds(1f);
        internal static WaitForSeconds TwoSeconds = new WaitForSeconds(2f);
        
        internal static WaitForSeconds ThreeSeconds = new WaitForSeconds(3f);
        internal static WaitForSeconds OneMinisecond = new WaitForSeconds(0.01f);
        internal static WaitForSeconds OneMillisecond = new WaitForSeconds(0.001f);
        internal static readonly TimeSpan tenSeconds = new TimeSpan(0, 0, 10);
        internal static readonly TimeSpan twentySeconds = new TimeSpan(0, 0, 20);
        internal static readonly Color inactive = new Color(235f / 255f, 195f / 255f, 136f / 255f);

        internal static LoginViewModel GetLoginModel(Credentials credentials)
        {
            return new LoginViewModel()
            {
                UserName = credentials.UserName,
                Password = credentials.Password
            };
        }

        internal static RegisterViewModel GetRegisterModel(params string[] credentials)
        {
            return new RegisterViewModel()
            {
                UserName = Encrypt(credentials[0], networkKey),
                Email = Encrypt(credentials[1], networkKey),
                Password = Encrypt(credentials[2], networkKey),
                ConfirmPassword = Encrypt(credentials[3], networkKey)
            };
        }

        internal static PuzzleFinishModel GetPuzzleFinishModel(PuzzleDto puzzle, int solutionKey, bool isResolved)
        {
            return new PuzzleFinishModel()
            {
                PuzzleId = puzzle.Id,
                PlayerId = WoodChess.GetInstance().CurrPlayer.Id,
                SolutionKey = solutionKey,
                IsResolved = isResolved
            };
        }
    }
}