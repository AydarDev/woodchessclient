﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GO_Scripts
{
    public class ColorDropDown : DropDownHandler
    {
        public void LoadSprites(Sprite[] sprites)
        {
            dropdown.ClearOptions();

            List<Dropdown.OptionData> options = new List<Dropdown.OptionData>();
            foreach (var figure in sprites)
                options.Add(new Dropdown.OptionData(figure));

            dropdown.AddOptions(options);
        }
    }
}
