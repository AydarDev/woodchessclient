﻿using System.Text;
using Globals;
using UnityEngine;
using UnityEngine.UI;
using WebSocketModels.Models;

namespace GO_Scripts
{
    public class ChallengeInfo : MonoBehaviour
    {
        public ChallengeDto challenge;
        public GameObject ChallengeAvatarObject;
        public GameObject AvatarPanelObject;
        public GameObject ChallengePanel;

        public void ViewChallengeInfo()
        {
            GameObject avatar = Instantiate(ChallengeAvatarObject, AvatarPanelObject.transform);
            avatar.GetComponent<Avatar>().ViewAvatar(challenge.FromPlayer, challenge.RatingType, challenge.Color);
            ChallengePanel.GetComponent<Text>().text = GetChallengeString();
        }

        private string GetChallengeString()
        {
            StringBuilder challengeLine = new StringBuilder();
            challengeLine.Append(GetPlayerName())
                .Append(" ")
                .Append(challenge.TimeControl.TimeRange)
                .Append("' + ")
                .Append(challenge.TimeControl.TimeIncrement)
                .Append("''");

            return challengeLine.ToString();
        }

        private StringBuilder GetPlayerName()
        {
            StringBuilder challengeName = new StringBuilder();
            
            if (challenge.FromPlayer.Equals(WoodChess.GetInstance().CurrPlayer))
                if (challenge.ToPlayer.IsNull)
                    challengeName.Append("Public");
                else
                    challengeName.Append("To ")
                        .Append(challenge.ToPlayer.Name);
            else
                challengeName.Append(challenge.FromPlayer.Name);

            return challengeName;
        }
    }
}
