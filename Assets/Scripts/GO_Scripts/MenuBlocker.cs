﻿using UnityEngine;

namespace GO_Scripts
{
    public class MenuBlocker : MonoBehaviour
    {
        private void Start()
        {
            Camera camera = GameObject.Find("Main Camera").GetComponent<Camera>();
            GameObject mainCanvas = GameObject.Find("MainCanvas");
            GameObject sceneCanvas = GameObject.Find("SceneCanvas");

            this.GetComponent<RectTransform>().sizeDelta = new Vector2(camera.pixelWidth, camera.pixelHeight);
            this.transform.localScale = new Vector2(1 /  sceneCanvas.transform.localScale.x, 1 / sceneCanvas.transform.localScale.y);
        }

    }
}
