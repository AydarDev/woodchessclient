﻿using System;
using ChessRules;
using Configuration;
using Globals;
using Helpers;
using Scenes.MainScene;
using UnityEngine;
using UnityEngine.UI;
using WebSocketModels;
using WebSocketModels.Enums;
using WebSocketModels.Models;
using static Helpers.AvatarHelper;

namespace GO_Scripts
{
    public class CreateChallengeDialog : MonoBehaviour
    {
        public PlayerDto player;

        private MainScene mainScene;
        private WoodChess woodChess => WoodChess.GetInstance();
        private NetworkClient networkClient => NetworkClient.GetInstance();
        private FavoriteChallenge favoriteChallenge => woodChess.GameConfiguration.FavoriteChallenge;

        private Dropdown colorDropdown;
        private InputField timeInput;
        private InputField incrementInput;
        private Image menuBlocker;
        TimeControlDto timeControl;


        private void Start()
        {
            LoadDropDownFigures();

            mainScene = GameObject.Find("SceneCanvas").GetComponent<MainScene>();
            GameObject.Find("ConfirmButton").GetComponent<ButtonScript>().action = OnSendChallengePush;
            GameObject.Find("CancelButton").GetComponent<ButtonScript>().action = OnCancelChallengePush;
            timeInput = GameObject.Find("TimeInputField").GetComponent<InputField>();
            incrementInput = GameObject.Find("IncrementInputField").GetComponent<InputField>();
            colorDropdown = GameObject.Find("ColorDropdown").GetComponent<Dropdown>();
            menuBlocker = GameObject.Find("MainCanvas").transform.GetChild(0).transform.Find("MenuBlocker").
                GetComponent<Image>();

            colorDropdown.value = (int)favoriteChallenge.Color;
            timeInput.text = favoriteChallenge.TimeRange.ToString();
            incrementInput.text = favoriteChallenge.Increment.ToString();

    }

        private void LoadDropDownFigures()
        {
            Sprite[] sprites = new Sprite[3];
            sprites[0] = Resources.Load<Sprite>(GetAvatarResource(woodChess.CurrPlayer, RatingType.Blitz, SideColor.None));
            sprites[1] = Resources.Load<Sprite>(GetAvatarResource(woodChess.CurrPlayer, RatingType.Blitz, SideColor.White));
            sprites[2] = Resources.Load<Sprite>(GetAvatarResource(woodChess.CurrPlayer, RatingType.Blitz, SideColor.Black));

            GameObject.Find("ColorDropdown").GetComponent<ColorDropDown>().LoadSprites(sprites);
        }

        internal void SetPlayerData(PlayerDto player)
        {
            GameObject playerInfo = GameObject.Find("HighPanel").transform.Find("PlayerInfo").gameObject;
            this.player = player;

            if (player != null)
                playerInfo.GetComponent<Text>().text = player.Name + " (" + GetRatingText(player[RatingType.Blitz]) + ")";
            else
                playerInfo.GetComponent<Text>().text = "Public challenge";
        }

        public void OnSendChallengePush()
        {
            int timeRange = Int32.Parse(timeInput.text);
            int increment = Int32.Parse(incrementInput.text);
            timeControl = new TimeControlDto(timeRange, increment);

            if (player != null)
                networkClient.CreateChallenge(timeControl, (SideColor)colorDropdown.value, player.Id);
            else
            {
                networkClient.CreateChallenge(timeControl, (SideColor)colorDropdown.value);
                SaveFavoriteChallenge((SideColor)colorDropdown.value, timeRange, increment);
            }
            
            Destroy(this.gameObject);
        }

        private void SaveFavoriteChallenge(SideColor color, int timeRange, int increment)
        {
            woodChess.GameConfiguration.FavoriteChallenge = new FavoriteChallenge
            {
                Color = color,
                TimeRange = timeRange,
                Increment = increment
            };
            ConfigurationHelper.SaveConfiguration();
        }


        public void OnCancelChallengePush()
        {
            Destroy(this.gameObject);
        }


        private void OnDestroy()
        {
            menuBlocker.enabled = false;
            mainScene.GetComponent<MainScene>().IsDialog = false;
        }
    }
}
