﻿using Helpers;
using Scenes.MainScene;
using UnityEngine;
using UnityEngine.UI;
using WebSocketModels;
using WebSocketModels.Models;

namespace GO_Scripts
{
    public class InputChallengeDialog : MonoBehaviour
    {
        private MainScene mainScene;
        private Image menuBlocker;
        private NetworkClient networkClient => NetworkClient.GetInstance();
        public ChallengeDto challenge;

        private void Start()
        {
            mainScene = GameObject.Find("SceneCanvas").GetComponent<MainScene>();
            GameObject.Find("AcceptButton").GetComponent<ButtonScript>().action = AcceptChallenge;
            GameObject.Find("DeclineButton").GetComponent<ButtonScript>().action = DeclineChallenge;
            menuBlocker = GameObject.Find("MainCanvas").transform.GetChild(0).transform.Find("MenuBlocker").
                GetComponent<Image>();
            
        }

        internal void SetChallenge(ChallengeDto challenge)
        {
            this.challenge = challenge;
            GameObject.Find("Caption").GetComponent<Text>().text =
            "Challenge " + challenge.TimeControl.TimeRange + "' + " + challenge.TimeControl.TimeIncrement + "''";

            GameObject.Find("LowPanel").transform.Find("ChallengeInfo").GetComponent<Text>().text =
                challenge.FromPlayer.Name + "(" + AvatarHelper.GetRatingText(challenge.FromPlayer[challenge.RatingType]) + ")";

            GameObject.Find("ChallengeAva").GetComponent<Image>().sprite =
                Resources.Load<Sprite>(AvatarHelper.GetAvatarResource(challenge.FromPlayer, challenge.RatingType, challenge.Color));
        }

        internal void AcceptChallenge()
        {
            networkClient.AcceptChallenge(challenge.Id);
        }

        internal void DeclineChallenge()
        {            
            networkClient.DeclineChallenge(challenge.Id);
            Destroy(this.gameObject);
        }

        private void OnDestroy()
        {
            menuBlocker.enabled = false;
            mainScene.GetComponent<MainScene>().IsDialog = false;
        }
    }
}
