﻿using System;
using UnityEngine;

namespace GO_Scripts
{
    public class ConfirmData
    {
        internal string ConfirmQuestion { get; set; }
        internal Sprite ActionSprite { get; set; }
        internal Action Action { get; set; }
        internal Action CancelAction { get; set; }
    }
}