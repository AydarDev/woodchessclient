﻿using System;
using GO_Scripts.Toggles;
using UnityEngine;
using UnityEngine.UI;

namespace GO_Scripts.ConfirmMenu
{
    public class ConfirmMenuScript : MonoBehaviour
    {
        private Action action;
        private Action cancelAction;
        public GameObject confirmButton;
        public GameObject cancelButton;
        private Button currButton;
        private Image confirmImage => transform.Find("CaptionCanvas").transform.Find("ActionImage").GetComponent<Image>();
        private Text confirmQuestion => transform.Find("CaptionCanvas").transform.Find("ActionConfirmation").GetComponent<Text>();

        public void Start()
        {
            confirmButton.GetComponent<ButtonScript>().action = Confirm;
            cancelButton.GetComponent<ButtonScript>().action = Cancel;
        }

        internal void Show(ConfirmData data)
        {
            action = data.Action;
            cancelAction = data.CancelAction;
            confirmImage.sprite = data.ActionSprite;
            confirmQuestion.text = data.ConfirmQuestion;
            gameObject.GetComponent<Canvas>().enabled = true;
        }
        
        internal void Show(ButtonWithConfirm button)
        {
            currButton = button.GetComponent<Button>();
            action = button.action;
            confirmImage.sprite = button.GetComponent<Image>().sprite;
            confirmQuestion.text = button.confirmQuestion;
            gameObject.GetComponent<Canvas>().enabled = true;
           
        }

        private void Cancel()
        {
            cancelAction?.Invoke();
            Hide();
        }

        private void Confirm()
        {
            action?.Invoke();
            Hide();
        }

        internal void Hide()
        {
            gameObject.GetComponent<Canvas>().enabled = false;
        }
    }
}