﻿using System.Net.WebSockets;
using ChessRules;
using Globals;
using Helpers;
using NetworkServices;
using UnityEngine;
using UnityEngine.UI;
using WebSocketModels.Enums;
using WebSocketModels.Models;

namespace GO_Scripts
{
    public class Avatar : MonoBehaviour
    {
        private WoodChess woodChess;
        private bool IsCurrPlayer => woodChess.CurrPlayer.Equals(player);
        public bool isViewStatus;
        private Image playerAvatar => GetComponent<Image>();
        private Image playerStatus => transform.Find("PlayerStatus").gameObject.GetComponent<Image>();
        private PlayerDto player;
        private WebSocket webSocket => WsClient.Instance.WebSocket;

        private void Awake()
        {
            woodChess = WoodChess.GetInstance();
        }

        private void Update()
        {
            if(IsCurrPlayer && isViewStatus)
                ViewWsStatus();
        }

        internal void ViewPlayerStatus(PlayerDto inputPlayer)
        {
            if (!inputPlayer.Equals(this.player)) return;
            //Debug.Log($"Viewing status of {inputPlayer.Name}, IsOnline: {inputPlayer.IsOnline}");
            playerStatus.enabled = inputPlayer.IsOnline;
        }

        public void ViewAvatar(PlayerDto inputPlayer, RatingType ratingType, SideColor color = SideColor.White)
        {
            player = inputPlayer;
            if(!IsCurrPlayer && isViewStatus) ViewPlayerStatus(inputPlayer);
            
            string playerAva = AvatarHelper.GetAvatarResource(inputPlayer, ratingType, color);
            playerAvatar.sprite = Resources.Load<Sprite>(playerAva);
        }
        
        public void ViewAvatar(PlayerDto inputPlayer, SideColor color = SideColor.White)
        {
            player = inputPlayer;
            if(!IsCurrPlayer && isViewStatus) ViewPlayerStatus(inputPlayer);
            
            string playerAva = AvatarHelper.GetAvatarResource(inputPlayer, RatingType.Blitz, color);
            playerAvatar.sprite = Resources.Load<Sprite>(playerAva);
        }
        
        private void ViewWsStatus()
        {
            playerStatus.enabled = 
                webSocket != null && webSocket.State == WebSocketState.Open;
        }
    }
}