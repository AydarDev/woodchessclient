﻿using Boards;
using ChessRules;
using UnityEngine;
using UnityEngine.UI;

namespace GO_Scripts
{
    public class Figure : MonoBehaviour
    {
        internal Square CurrentSquare;
        internal string FigureSquare => name + CurrentSquare.name;
        internal UnityBoard Board;
        internal SideColor Color;
        
        private readonly Vector2 normalScale = new Vector2(50, 50);
        private readonly Vector2 largeScale = new Vector2(80, 80);
        private Vector2 initialPosition;
        private Vector2 clickPosition;
        private bool dragging;
        private bool validPick;
        internal bool IsSelected { get; set; }
        internal bool IsPromotionFigure { get; set; }

        public void SetFigureInfo(string figure)
        {
            name = figure;
            int fig = figure[0];
            if (fig >= 66 && fig <= 82)
                Color = SideColor.White;
            else if (fig >= 98 && fig <= 114)
                Color = SideColor.Black;
            else
                Color = SideColor.None;
        }

        public void PointerDown()
        {
            validPick = Board.IsValidPick(this);
            if (Board.IsPromote)
            {
                if(!IsPromotionFigure) return;
                Board.ChoosePromotionFigure(this);
                return;
            }
            if (validPick)
            {
                IsSelected = !IsSelected;
                GetComponent<RectTransform>().localScale = largeScale;
                initialPosition = transform.position;
                transform.position = GetClickPosition();
                if (!IsSelected) return;
                Board.SelectFigure(this);
            }
            else Board.PickFigure(this);
        }

        public void PointerUp()
        {
            if (!validPick || Board.IsPromote || dragging) return;
            GetComponent<RectTransform>().localScale = normalScale;
            if(!IsSelected) Board.UnselectFigure();
            transform.position = initialPosition;
        }

        public void DragStart()
        {
            if (Board.IsPromote) return;
            validPick = Board.IsValidPick(this);
            if (!validPick)return;
            IsSelected = true;
            dragging = true;
        }
        public void Drag()
        {
            if (!validPick || Board.IsPromote) return;
            transform.position = GetClickPosition();
        }
        public void DragFinish()
        {
            if (!validPick || Board.IsPromote) return;
            dragging = false;
            
            GetComponent<RectTransform>().localScale = normalScale;
            if(!OnBoard(transform.position))
            {
                Board.ResetObject(this);
                return;
            }    
            
            var squareName = Board.VectorToSquare(transform.position);
            var square = Board.Squares[squareName];
            if (square.Equals(CurrentSquare))
            {
                transform.position = initialPosition;
                return;
            }
            IsSelected = false;
            Board.DropFigure(square);
        }

        private Vector2 GetClickPosition()
        {
            return Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
        
        private bool OnBoard(Vector2 vector)
        {
            bool result = vector.x >= -8 & vector.x <= 8 & vector.y >= -8 & vector.y <= 8;
            return result;
        }
    }
}