﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace GO_Scripts
{
    public class TimeInputField : MonoBehaviour
    {
        public InputField inputField;
        public Boolean isZeroAllowed;

        private void Start()
        {
            inputField.onValidateInput = onValidateInput;
        }

        private char onValidateInput(string text, int charIndex, char addedChar)
        {
            if (text.Length == 0 && addedChar == '0' && !isZeroAllowed)
                return '\0';

            if (isZeroAllowed && "0".Equals(text))
                return '\0';

            if (addedChar < '0' || addedChar > '9')
                return '\0';

            if (!"".Equals(text) && Int32.Parse(text + addedChar) > 50)
                return '\0';

            return addedChar;
        }
    }
}
