﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace GO_Scripts
{
    public class ToggleScript : MonoBehaviour
    {
        public Toggle toggle;
        public Action action;

        public virtual void OnValueChange()
        {
            action.Invoke();
        }        
    }
}
