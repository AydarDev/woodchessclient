﻿using GO_Scripts.ConfirmMenu;
using UnityEngine;
using UnityEngine.UI;

namespace GO_Scripts.Toggles
{
    public class ButtonWithConfirm : CustomToggle
    {
        public GameObject confirmMenu;
        public string confirmQuestion;

        public void ShowConfirmMenu()
        {
            confirmMenu.GetComponent<ConfirmMenuScript>().Show(this);
        }
        public void Block()
        {
            IsBlocked = true;
            button.GetComponent<Button>().interactable = false;
        }
        public void Unblock()
        {
            IsBlocked = false;
            button.GetComponent<Button>().interactable = true;
        }
    }
}