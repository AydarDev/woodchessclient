﻿using UnityEngine;
using UnityEngine.UI;

namespace GO_Scripts.Toggles
{
    public class CustomToggle : ButtonScript
    {
        public Sprite enabledImage;
        public Sprite disableImage;
        internal bool IsOn { get; set; }
        internal bool IsBlocked { get; set; }

        public void ChangeValue()
        {
            IsOn = !IsOn;
            button.GetComponent<Image>().sprite = IsOn ? enabledImage : disableImage;
        }
    }
}