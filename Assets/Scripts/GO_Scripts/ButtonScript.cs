﻿using System;
using UnityEngine;

namespace GO_Scripts
{
    public class ButtonScript : MonoBehaviour
    {
        public Action holdAction;
        public Action action;
        public GameObject button;

        public virtual void OnClick()
        {
            action?.Invoke();
        }

        public virtual void OnHold()
        {
            holdAction?.Invoke();
        }
    }
}
