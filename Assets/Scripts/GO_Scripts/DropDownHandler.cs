﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GO_Scripts
{
    public class DropDownHandler : MonoBehaviour
    {
        public Dropdown dropdown;
        public UnityAction<int> action;
        internal bool isShow = false;
        private int currValue;

        public void OnMenuClick()
        {
            if (isShow)
                dropdown.Hide();

            isShow = !isShow;
        }

        public void OnValueChoose()
        {
            if(currValue != dropdown.value)
            {
                action?.Invoke(dropdown.value);
                currValue = dropdown.value;
            }
            isShow = false;
        }

        public void SetValueWithoutNotify(int value)
        {
            currValue = value;
            dropdown.SetValueWithoutNotify(value);
        }
    }
}
