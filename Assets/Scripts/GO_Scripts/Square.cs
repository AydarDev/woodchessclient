﻿using Boards;
using ChessRules;
using UnityEngine;

namespace GO_Scripts
{
    public class Square : MonoBehaviour
    {
        public int x;
        public int y;
        public SideColor color;
        internal UnityBoard board;
        public Figure CurrentFigure;

        public void Click()
        {
            if (board.IsFigureSelected)
                board.DropFigure(this);
        }
    }
}