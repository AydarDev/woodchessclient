﻿using System;
using System.Collections;
using Assets.Scripts;
using ChessRules;
using Globals;
using UnityEngine;
using UnityEngine.UI;
using static Helpers.CommonHelper;

namespace GO_Scripts
{
    public class TimerScript : MonoBehaviour
    {
        public GameObject TimerScreen;
        public GameObject TimerValue;
        public GameObject TimeRunIcon;
        
        public Timer Timer;
        public SideColor color;
        

        private void Start()
        {
            StartCoroutine(ShowTime(TimerTick));
        }

        private IEnumerator ShowTime(Action timerTick)
        {
            yield return OneMillisecond;
            while (true)
            {
                yield return OneMinisecond;
                timerTick();
            }
        }


        private void TimerTick()
        {
            try
            {
                if (Timer != null)
                {
                    //Log("Updating timer value");
                    ViewTimerIsActive();
                    UpdateTimerValue();
                }
                else
                {
                    Log("Timer is null");             
                }
            }
            catch (Exception e)
            {
                Log(e.Message);
            }
        }

        private void UpdateTimerValue()
        {
            TimerValue.GetComponent<Text>().text = Timer.CurrTime.ToString(GetDisplayFormat(Timer.CurrTime));
        }

        private void ViewTimerIsActive()
        {
            if (Timer.IsRun)
            {
                TimerScreen.GetComponent<Image>().sprite = Resources.Load<Sprite>("Clocks/ClockActive");
                TimerValue.GetComponent<Text>().color = Color.black;
                TimerValue.GetComponent<Text>().fontStyle = FontStyle.Bold;
                TimeRunIcon.GetComponent<Image>().enabled = true;
            }
            else
            {
                TimerScreen.GetComponent<Image>().sprite = Resources.Load<Sprite>("Clocks/ClockInactive");;
                TimerValue.GetComponent<Text>().color = Color.white;
                TimerValue.GetComponent<Text>().fontStyle = FontStyle.Normal;
                TimeRunIcon.GetComponent<Image>().enabled = false;
            }
        }
        
        
        string GetDisplayFormat(TimeSpan time)
        {
            string format = @"mm\:ss";
            if (time <= twentySeconds && time >= tenSeconds)
                format = @"ss\.f";
            else if (time < tenSeconds)
                format = @"s\.f";

            return format;
        }

        private void Log(string message)
        {
            if (WoodChess.GetInstance().GameConfiguration.SystemSettings.DevMode)
                GameObject.Find("StatusText").GetComponent<Text>().text = message;
        }
    }
}