﻿using System;
using System.Globalization;
using System.Text;
using ChessRules;
using Globals;
using Helpers;
using Scenes.BoardScene;
using UnityEngine;
using UnityEngine.UI;
using WebSocketModels.Models;

namespace GO_Scripts
{
    public class PlayerPanel : MonoBehaviour
    {
        internal PlayerDto Player;
        internal SideColor Color { get; set; }
        private Avatar playerAvatar => transform.Find("PlayerAvatar").GetComponent<Avatar>();
        private Transform playerInfo => transform.Find("PlayerInfo").transform;
        private Text playerName => playerInfo.Find("PlayerName").GetComponent<Text>();
        private Text playerRating => playerInfo.Find("PlayerRating").GetComponent<Text>();
        private BoardScene boardScene => GameObject.Find("SceneCanvas").GetComponent<BoardScene>();
        private TimerScript playerTimer => transform.Find("Timer").GetComponent<TimerScript>();

        private GameDto currGame => WoodChess.GetInstance().CurrGame;

        internal void SetPlayerData(PlayerDto player)
        {
            Player = player;
            ViewPlayerInfo();
            SetUpTimer();
        }

        private void ViewPlayerInfo()
        {
            playerName.text = Player.Name;
            playerRating.text = Math.Round(Player[currGame.RatingType], MidpointRounding.AwayFromZero).ToString(CultureInfo.InvariantCulture);
            playerAvatar.ViewAvatar(Player, currGame.RatingType, Color);
        }

        internal void UpdatePlayerInfo(PlayerDto inputPlayer)
        {
            if (!Player.Equals(inputPlayer)) return;
            Player = inputPlayer;
            ViewPlayerInfo();
        }
        
        private void SetUpTimer()
        {
           playerTimer.Timer =  boardScene.ChessClock.Timers[Color];
        }

        //TODO: Remove copy-paste
        internal void ShowRatingDeviation(double deviation)
        {
            var builder = new StringBuilder();
            builder.Append(playerRating.text)
                .Append("(")
                .Append(deviation >= 0 ? "+" : "")
                .Append(AvatarHelper.GetRatingText(deviation))
                .Append(")");

            playerRating.text = builder.ToString();
        }
    }
}