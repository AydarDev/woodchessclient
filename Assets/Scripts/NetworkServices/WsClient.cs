﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Configuration;
using Globals;
using Newtonsoft.Json;
using UnityEngine;
using WebSocketModels;
using WebSocketModels.WsHandlers;
using WebSocketModels.WsMessages.WsResponses;
using WebSocketModels.WsReceivers;

namespace NetworkServices
{
    public class WsClient : MonoBehaviour
    {
        public static WsClient Instance;
        readonly ConcurrentQueue<string> inputWsMessages = new ConcurrentQueue<string>();
        string inputWsMessageJson;
        private bool isMonitoring;
        private NetworkSettings networkSettings => WoodChess.GetInstance().GameConfiguration.NetworkSettings;
        private SystemSettings systemSettings => WoodChess.GetInstance().GameConfiguration.SystemSettings;

        private readonly object locker = new object();

        internal IReceiver WsReceiver;

        private bool isListening;

        private Coroutine listenCoroutine;
        private readonly ArraySegment<byte> pingArraySegment = new ArraySegment<byte>(new byte[] { 5, 7 }, 0, 2);
        private int receiveBufferSize = 32_768;

        private int KeepAliveCounter
        {
            get
            {
                lock (locker)
                    return counterValue;
            }
            set
            {
                lock (locker)
                    counterValue = value;
            }
        }

        private int counterValue;

        internal ClientWebSocket WebSocket { get; private set; }

        private readonly WaitForSeconds keepAlivePeriod = new WaitForSeconds(1f);
        private const int KeepAliveWaitPeriod = 3;
        readonly WaitForSeconds halfSecond = new WaitForSeconds(0.5f);
        readonly WaitForSeconds waitPeriod = new WaitForSeconds(0.1f);

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                StartCoroutine(InitWebSocket());
            }
            else if (Instance == this)
                Destroy(gameObject);

            DontDestroyOnLoad(gameObject);
        }


        private void Update()
        {
            if (WsReceiver != null) ProcessWsMessage();
        }

        private void ProcessWsMessage()
        {
            try
            {
                if (inputWsMessages.Count == 0)
                    return;

                inputWsMessages.TryDequeue(out inputWsMessageJson);
                Debug.Log($"Input message json: {inputWsMessageJson}");
                WsResponse message =
                    JsonConvert.DeserializeObject<WsResponse>(inputWsMessageJson, WsResponseConverter.GetInstance());
                IWsHandler wsHandler = WsHandlerFactory.GetWsHandler(message);

                if (wsHandler != null)
                    wsHandler.ProcessWsMessage(WsReceiver, message);
            }
            catch (Exception e)
            {
                if (WsReceiver != null)
                    WsReceiver.ShowMessage(e.Message);
                Debug.LogError(e.Message + e.StackTrace);
            }
        }

        private IEnumerator InitWebSocket()
        {
            ResetKeepAliveCounter();

            yield return TryToConnectWs();
            if (WebSocket == null || WebSocket.State != WebSocketState.Open)
                yield break;

            if (!isListening)
                listenCoroutine = StartCoroutine(ListenWebsocketCoroutine());

            if (!isMonitoring && !systemSettings.DevMode)
                yield return MonitorConnection();
        }

        private IEnumerator TryToConnectWs()
        {
            NetworkClient.GetInstance().SendRequestAction = SendMessage;
            while ((WebSocket == null ||
                    (WebSocket.State != WebSocketState.Open && WebSocket.State != WebSocketState.Connecting)))
            {
                int counter = 0;
                WebSocket = new ClientWebSocket();
                WebSocket.Options.AddSubProtocol(WoodChess.GetInstance().Token);
                WebSocket.Options.AddSubProtocol(WoodChess.GetInstance().CurrPlayer.Id.ToString());
                WebSocket.Options.SetBuffer(receiveBufferSize: receiveBufferSize, sendBufferSize: 4096);
                var serverUrl = networkSettings.WebSocketServerAddress;
                if (WebSocket.State != WebSocketState.Connecting && WebSocket.State != WebSocketState.Open)
                {
                    //Debug.Log($"Trying to connect to {serverUrl}");
                    WebSocket.ConnectAsync(new Uri(networkSettings.WebSocketServerAddress), CancellationToken.None);
                }

                //Debug.Log("Waiting for Websocket open...");
                while (counter++ < 10)
                {
                    yield return halfSecond;
                    if (WebSocket.State == WebSocketState.Open)
                    {
                        //Debug.Log("WebSocket connected!");
                        yield break;
                    }
                }

                //Debug.Log("WebSocket cannot connect to server");
                WebSocket.Abort();
                WebSocket = null;
            }

            //Debug.Log("Cannot connect to server!");
        }

        private IEnumerator ListenWebsocketCoroutine()
        {
            yield return ListenWebSocketAsync();
        }

        private async Task ListenWebSocketAsync()
        {
            //Log("Listening Websocket.");
            isListening = true;
            var buffer = new byte[receiveBufferSize];
            while (WebSocket.State == WebSocketState.Open)
            {
                WebSocketReceiveResult result =
                    await WebSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);

                ReceiveWsData(result, buffer);
            }

            isListening = false;
        }

        private void ReceiveWsData(WebSocketReceiveResult result, byte[] buffer)
        {
            if (result.MessageType == WebSocketMessageType.Text)
            {
                string message = Encoding.UTF8.GetString(buffer, 0, result.Count);
                inputWsMessages.Enqueue(message);
            }

            if (result.MessageType == WebSocketMessageType.Binary && IsMatchPingPattern(buffer)) 
                ResetKeepAliveCounter();

            if (result.MessageType == WebSocketMessageType.Close) 
                CloseConnection();
        }

        private static bool IsMatchPingPattern(byte[] buffer)
        {
            return buffer[0] == 7 && buffer[1] == 5;
        }

        private IEnumerator MonitorConnection()
        {
            while (WebSocket == null || WebSocket.State != WebSocketState.Open || !isListening)
                yield return waitPeriod;

            Debug.Log("Websocket ready, start monitoring");

            isMonitoring = true;
            while (WebSocket != null && WebSocket.State == WebSocketState.Open)
            {
                yield return SendPingMessageAsync();
                yield return keepAlivePeriod;
                KeepAliveCounter--;
                if (KeepAliveCounter == 0)
                {
                    Debug.Log("Server not available. Disconnecting");
                    AbortConnection();
                    break;
                }
            }

            yield return InitWebSocket();
        }

        private new void SendMessage(string message)
        {
            if (!IsWebSocketReady())
            {
                //Debug.Log("WebSocket not ready. Waiting...");
                StartCoroutine(WaitForWsReady());
            }

            StartCoroutine(SendMessageCoroutine(message));
        }

        private bool IsWebSocketReady()
        {
            return WebSocket != null && WebSocket.State == WebSocketState.Open && isListening;
        }

        private IEnumerator SendMessageCoroutine(string message)
        {
            yield return SendMessageAsync(message);
        }

        private async Task SendMessageAsync(string message)
        {
            //Log($"Sending message {message}");

            if (WebSocket == null || WebSocket.State != WebSocketState.Open)
                return;

            await WebSocket.SendAsync(buffer: new ArraySegment<byte>(array: Encoding.UTF8.GetBytes(message),
                    offset: 0,
                    count: message.Length),
                messageType: WebSocketMessageType.Text,
                endOfMessage: true,
                cancellationToken: CancellationToken.None);
        }

        private IEnumerator WaitForWsReady()
        {
            int counter = 0;
            while (WebSocket == null || WebSocket.State != WebSocketState.Open || !isListening)
            {
                yield return waitPeriod;
                if (counter++ > 100)
                {
                    Log($"WebSocket connection error.");
                    throw new WebSocketException(WebSocketError.Faulted);
                }
            }
        }

        private async Task SendPingMessageAsync()
        {
            if (WebSocket == null || WebSocket.State != WebSocketState.Open)
                return;

            await WebSocket.SendAsync(buffer: pingArraySegment,
                messageType: WebSocketMessageType.Binary,
                endOfMessage: true,
                cancellationToken: CancellationToken.None);
        }

        internal void ResetKeepAliveCounter()
        {
            KeepAliveCounter = KeepAliveWaitPeriod;
        }

        public void CloseConnection()
        {
            if (WebSocket != null)
                WebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Close Connection",
                    CancellationToken.None);
            WebSocket = null;
            StopAllCoroutines();
        }

        private void AbortConnection()
        {
            StopCoroutine(listenCoroutine);
            WebSocket.Abort();
            WebSocket = null;

            isMonitoring = false;
            isListening = false;
        }

        private void Log(string message)
        {
            Debug.Log(message);
            if (WsReceiver != null && systemSettings.DevMode)
                WsReceiver.ShowMessage(message);
        }
    }
}