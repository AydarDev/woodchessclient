﻿using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.Networking;

namespace NetworkServices
{
    public class CustomCertHandler : CertificateHandler
    {
        private static string _pubKey = "3082010A0282010100CE388FA05071A99D21885D9A957B804DEAB0A8F4" +
                                        "7CA703F76F7493C680DD20FCBE6681FAB4BC9D8F55AA1B2FBAB87E2C79" +
                                        "D8C280427BCCCF10A6C761479E76937237AB5CA7AE1DE83A4F7AB16B76" +
                                        "AA96F0B3E3BE613DB2D14E3D4E1F7EC89455BE3AA44163B69A10863CF7" +
                                        "957087E116EEB787FC868FA103237FD61D2C9697321967481E10AB1275" +
                                        "D7C98C73B63E19072276552B8A86D6323F2F9BBEA75307E7146B947CA9" +
                                        "6DB2DAE82453377A608990120B065E064CD163283F0F42298BACC3EAAE" +
                                        "0517140865F5FAEA9AABDA4D27CB3CB6747D012F8681DE21C6B3B16374" +
                                        "C3057A0A0514C35F4F6CFE0B7C839721850F9DF3ED1BDAF48E429DAF28" +
                                        "20C7F9990203010001";
        
        protected override bool ValidateCertificate(byte[] certificateData)
        {
            X509Certificate2 certificate = new X509Certificate2(certificateData);
            string pk = certificate.GetPublicKeyString();
            Debug.Log(pk);
            if (pk != null && pk.Equals(_pubKey))
                return true;
            return false;
        }
    }
}