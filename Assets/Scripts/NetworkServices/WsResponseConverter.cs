﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebSocketModels.Enums;
using WebSocketModels.WsMessages.WsResponses;

namespace NetworkServices
{
    public class WsResponseConverter : JsonConverter
    {
        static WsResponseConverter _wsResponseConverter;

        private WsResponseConverter() { }

        public static WsResponseConverter GetInstance()
        {
            if (_wsResponseConverter == null)
                _wsResponseConverter = new WsResponseConverter();

            return _wsResponseConverter;
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(WsResponse).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jo = JObject.Load(reader);

            WsResponse item;
            MessageType responseType = (MessageType)((int)jo["MessageType"]);

            switch (responseType)
            {
                case (MessageType.Challenge):
                    item = new ChallengeResponse();
                    break;
                case (MessageType.Game):
                    item = new GameResponse();
                    break;
                case (MessageType.Player):
                    item = new PlayerResponse();
                    break;
                case (MessageType.Data):
                    item = new DataResponse();
                    break;
                default:
                    item = null;
                    break;
            }

            serializer.Populate(jo.CreateReader(), item);

            return item;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
