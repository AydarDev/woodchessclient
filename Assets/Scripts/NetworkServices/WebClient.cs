﻿using System;
using System.Collections;
using System.Collections.Generic;
using Configuration;
using Globals;
using Helpers;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using WebSocketModels.Enums;
using WebSocketModels.Models;
using WebSocketModels.Models.Puzzles;
using WebSocketModels.UserManagerModels;

namespace NetworkServices
{
    public class WebClient : MonoBehaviour
    {
        private string chessServer => $"{NetworkSettings.GameWebServerAddress}/chess_api";
        private string puzzleServer => $"{NetworkSettings.PuzzleWebServerAddress}/puzzle_api";
        private static WoodChess woodChess => WoodChess.GetInstance();
        private static NetworkSettings NetworkSettings => woodChess.GameConfiguration.NetworkSettings;
        private static WebClient _webClient;
        private string LoginRequest => $"{chessServer}/home/login";
        private string RegisterRequest => $"{chessServer}/home/register";
        private string GetAllPlayersRequest => $"{chessServer}/Players/GetAll";
        private string GetOnlinePlayersRequest => $"{chessServer}/Players/GetOnline";
        private string GetCurrentGameRequest => $"{chessServer}/Games/GetCurrent/";
        private string GetChallengesRequest => $"{chessServer}/Challenges/Get/";
        private string GetGameRequest => $"{chessServer}/Games/Get/";
        private string GetNextPuzzleRequest => $"{puzzleServer}/puzzles/GetNext";
        private string GetCsvPuzzleRequest => $"{puzzleServer}/puzzles/GetCsvPuzzle";
        private string FinishPuzzleRequest => $"{puzzleServer}/puzzles/Finish";

        public static WebClient Instance;

        private void Start()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else if (Instance == this)
                Destroy(gameObject);

            DontDestroyOnLoad(gameObject);
        }

        private long playerId => WoodChess.GetInstance().CurrPlayer.Id;

        internal void Login(LoginViewModel model, Action<UserManagerResponse> handleLogin)
            => StartCoroutine(SendPostRequest(LoginRequest, model, handleLogin));

        internal void Register(RegisterViewModel model, Action<UserManagerResponse> handleRegister)
            => StartCoroutine(SendPostRequest(RegisterRequest, model, handleRegister));

        internal void GetAllPlayers(Action<List<PlayerDto>> handleResult) 
            => StartCoroutine(SendGetRequest(GetAllPlayersRequest, handleResult));

        internal void GetOnlinePlayers(Action<List<PlayerDto>> handleResult) 
            => StartCoroutine(SendGetRequest(GetOnlinePlayersRequest, handleResult));

        internal void GetCurrentGame(long id, Action<GameDto> handle) 
            => StartCoroutine(SendGetRequest(GetCurrentGameRequest + id, handle));

        internal void GetGame(Guid id, Action<GameDto> handleResult) 
            => StartCoroutine(SendGetRequest(GetGameRequest + id, handleResult));

        internal void GetChallenges(long id, Action<List<ChallengeDto>> handleResult) 
            => StartCoroutine(SendGetRequest(GetChallengesRequest + id, handleResult));

        internal void GetNextPuzzle(PuzzleType type, Action<PuzzleDto> handleResult) 
            => StartCoroutine(SendGetRequest($"{GetNextPuzzleRequest}/{type}/{playerId}", handleResult));

        public void GetCsvPuzzle(Action<PuzzleDto> handleResult) 
            => StartCoroutine(SendGetRequest(GetCsvPuzzleRequest, handleResult));

        internal void FinishPuzzle(PuzzleFinishModel model)
            => StartCoroutine(SendPostRequest<object>(FinishPuzzleRequest, model, (_) => { }));

        private IEnumerator SendGetRequest<T>(string url, Action<T> handleResult)
        {
            using (var www = UnityWebRequest.Get(url))
            {
                yield return www.SendWebRequest();
                if (www.isNetworkError || www.isHttpError)
                    SystemHelper.Instance.ShowMessage(www.error);
                else
                {
                    string json = www.downloadHandler.text;
                    T response = default(T);
                    try
                    {
                        response = JsonConvert.DeserializeObject<T>(json);
                    }
                    catch (Exception e)
                    {
                        Debug.Log(e.Message);
                    }
                    finally
                    {
                        handleResult(response);
                    }
                }
            }
        }

        private IEnumerator SendPostRequest<T>(string url, object model, Action<T> handleResult)
        {
            string modelJson = JsonConvert.SerializeObject(model);
            byte[] payload = new System.Text.UTF8Encoding().GetBytes(modelJson);

            using (var www = new UnityWebRequest(url, "POST"))
            {
                www.uploadHandler = new UploadHandlerRaw(payload);
                www.downloadHandler = new DownloadHandlerBuffer();
                www.SetRequestHeader("Content-Type", "application/json");
                www.SetRequestHeader("cache-control", "no-cache");
                yield return www.SendWebRequest();

                if (www.isNetworkError ||www.isHttpError)
                    SystemHelper.Instance.ShowMessage(www.error);
                else
                {
                    string json = www.downloadHandler.text;
                    T response = default(T);
                    try
                    {
                        response = JsonConvert.DeserializeObject<T>(json);
                    }
                    catch (Exception e)
                    {
                        Debug.Log(e.Message);
                    }
                    finally
                    {
                        handleResult(response);
                    }
                }
            }
        }
    }
}